import React, { Component } from 'react';
import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import HomeScreen from './src/screens/HomeScreen';
import NoConnectivity from './src/screens/NoConnectivity';
import SplashScreen from './src/screens/SpalshScreen';
import Login from './src/screens/Login';
import SurveyList from './src/screens/SurveyList';
import CompanyList from './src/screens/CompanyList';
import LocationList from './src/screens/LocationList';
import LanguageSelection from './src/screens/LanguageSelection';
import SurveyStart from './src/screens/SurveyStart';
import SurveyQuestions from './src/screens/SurveyQuestions';
import Thankyou from './src/screens/Thankyou';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Drawer from './src/components/Drawer';

console.disableYellowBox = true;

export class App extends Component {

 render(){
   return(
      <View />
   )
 }
}

export const AppNavigator = createStackNavigator({
  HomeScreen:{
    navigationOptions:{
      header:null
    },
     screen:HomeScreen
  },
  NoConnectivity:{
    navigationOptions:{
      header:null
    },
     screen:NoConnectivity
  },
  Login: {
    navigationOptions:{
      header:null
    },
    screen:Login
  },
  SurveyList:{
    navigationOptions:{
      header:null
    },
    screen:SurveyList
  },
  CompanyList:{
    navigationOptions:{
      header:null
    },
    screen:CompanyList
  },
  LocationList:{
    navigationOptions:{
      header:null
    },
    screen:LocationList
  },
  SurveyStart:{
    navigationOptions:{
      header:null
    },
    screen:SurveyStart
  },
  Thankyou:{
    navigationOptions:{
      header:null
    },
    screen:Thankyou
  },
  SurveyQuestions:{
    navigationOptions:{
      header:null
    },
    screen:SurveyQuestions
  },
  SplashScreen:{
    navigationOptions:{
      header:null
    },
    screen:SplashScreen
  },
  LanguageSelection:{
    navigationOptions:{
      header:null
    },
    screen:LanguageSelection
  }
},{
  initialRouteName: 'SplashScreen',
  defaultNavigationOptions: {
    gesturesEnabled: false,
  }
}
);

const DrawerNavigatorExample = createDrawerNavigator({
  //Drawer Optons and indexing
  AppNavigator
}, {
  contentComponent: Drawer,
  drawerPosition: 'left',
  drawerOpacity: 1,
  defaultNavigationOptions: {
    header: null
  },
  navigationOptions: ({ navigation }) => ({
    header: null
  })
});


export default createAppContainer(DrawerNavigatorExample);

