let initialState = {
    PaymentHistoryData: [],
    type: null,
    reducer_message:''
}
export default function (state = initialState, action) {
    switch (action.type) {

        case "GET_PAYMENTHISTORY_PENDING": {
            return {
                ...state,
                type: action.type
            }
        }

        case "GET_PAYMENTHISTORY_SUCCESS": {
            return {
                ...state,
                PaymentHistoryData: action.body.data.data,
                type: action.type
            }
        }

        case "GET_PAYMENTHISTORY_FAILD": {
            return {
                ...state,
                reducer_message: action.body.message[0],
                type: action.type
            }
        }
        case "SOMETHING_WENT_WRONG":{
            return{
              ...state,
              type:action.type
            }
          }

        default:
            return state;
    }

}










