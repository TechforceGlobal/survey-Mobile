let initialState = {
  username:'',
  reducer_message:'',
  type:null,
  user_logindata:null,
  user_login_id:null,
  user_login_token:null,
  currentLanguageCode:'en',
  userProfile:null,
  JsonData:null,
  catagory_list:[],
  subCategory_list:[],
  all_auction_data:[],
  all_auction_list:[],
  auction_detail:[],
  auction_bid:null,
  auction_bid_message:null,
  auction_bid_status_code:null,
  current_bid_price:null,
  current_proxy_bid_price:null,
  history_bid_data:null,
  get_all_wish_list_item:null,
  deposit_checkout:null,
  submit_deposit_checkout:null,
  auctionQuestion:null,
  auctionCheckout:null,
  auctionCheckout_data:null,
  buynow_checkout_data:null,
  get_item_shipping:null,
  addresslist_data:null,
  buynow_order:null,
  close_envelope_bid:null,
  get_country:null,
  get_state: null,
  get_city: null,
  get_isd:null,
  submit_close_en:null,
  get_private_auction:null,
  buyer_invoice:null,
  buyer_invoice_detail:null,
  updatedProfile:[],
  add_payment_wallet:null,
  get_payment_wallet_history_data:null,
  active_list : [],
  won_list: [],
  not_won_list:[],
  get_feedack_list:[],
  get_dashboard:[],
  buyerProfile:[],
  buynowlist:[],
  allLabels:null,
  deposit_status_code:null
}
export default function(state=initialState, action){
  switch (action.type) {
    case "DISPLAY_USERNAME":
    {
      return {
        ...state,
        username:action.username,
        type:action.type
      }
    }

    case "CHANGE_LANGUAGE":
    {
      return {
        ...state,
        type:action.type,
        currentLanguageCode:action.language_code
      }
    }

    case "LOGOUT_SUCCESS":
      {
        return{
          ...state,
          username:'',
          reducer_message:'',
          type:action.type,
          user_logindata:null,
          user_login_id:null,
          user_login_token:null,
          userProfile:null
        }
      }

    case "REGISTER_PENDING":
    {
      return {
        ...state,
        type:action.type
      }
    }
    case "REGISTER_SUCCESS":
    {
      return {
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }
    case "REGISTER_FAILD":
    {
      return {
        ...state,
        reducer_message:action.error,
        type:action.type
      }
    }

    case "LOGIN_PENDING":
    {
      return {
        ...state,
        type:action.type
      }
    }
    case "LOGIN_SUCCESS":
    {
      return {
        ...state,
        user_logindata:action.body,
        user_login_id:action.body.data.customer_data.id,
        user_login_token:action.body.data.token,
        currentLanguageCode:action.body.data.customer_data.lang_code,
        type:action.type,
        userProfile:action.body.status_code === 200 ? action.body.data.customer_data : null
      }
    }
    case "LOGIN_FAILD":{
      return {
        ...state,
        reducer_message:action.error,
        type:action.type
      }
    }
    
    case "GETCATAGORY__PENDING":{
        return{
          ...state,
          type:action.type
        }
      }
      case "GETCATAGORY_SUCCESS":{
        return{
          ...state,
          catagory_list:action.body,
          type:action.type
        }
      }
      case "GETCATAGORY_FAILD":{
        return{
          ...state,
          reducer_message:action.error,
          type:action.type
        }
      }

      case "GETSUBCATAGORY_PENDING":{
        return{
          ...state,
          type:action.type
        }
      }
      case "GETSUBCATAGORY_SUCCESS":{
        return{
          ...state,
          subCategory_list:action.body.data.length!==0 ?  action.body.data.category : [],
          type:action.type
        }
      }
      case "GETSUBCATAGORY_FAILD":{
        return{
          ...state,
          reducer_message:action.error,
          type:action.type
        }
      }

      case "GETALLAUCTION_PENDING" :{
        return{
          ...state,
          type:action.type
        }
      }
      case "NO_INTERNET" :{
        return{
          ...state,
          type:action.type
        }
      }
      case "GETALLAUCTION_SUCCESS":{
        /* let actionData = [];
        if(action.body.status_code === 200){
          actionData = action.body.data.product_data;
        }
        let dataDirection = action.dataDirection;
        let all_auction_list = [...state.all_auction_list];
        let noOfcurrentRecords = all_auction_list.length;
        let noOfNewRecords = actionData.length;

        if(action.body.data.length!==0 && dataDirection==='next'){
          if(noOfcurrentRecords+noOfNewRecords>10){
            all_auction_list.splice(0,noOfcurrentRecords+noOfNewRecords-10)
            for (let i = 0; i < actionData.length; i++) {
              all_auction_list.push(actionData[i])
            }
          }else{
            for (let i = 0; i < actionData.length; i++) {
              all_auction_list.push(actionData[i])
            }
          }
          //console.log('all_auction_list with pagination: ' + JSON.stringify(all_auction_list))
        }else{
          for (let i = 0; i < actionData.length; i++) {
            all_auction_list.unshift(actionData[i])
          }
        } */
        return{
          ...state,
          all_auction_data:action.body,
          type:action.type,
          //all_auction_list: all_auction_list
          all_auction_list:action.body.status_code === 200 ? action.body.data.product_data : []
        }
      }
      case "GETALLAUCTION_FAILD":{
        return{
          ...state,
          reducer_message:action.error,
          type:action.type
        }
      }
      case "GETAUCTION_PENDING":{
          return{
            ...state,
            type:action.type
          }
      }
      case "GETAUCTION_SUCCESS":{
          return{
            ...state,
            auction_detail:action.body.data,
            type:action.type
          }
      }
      case "GETAUCTION_FAILD":{
        return{
          ...state,
          reducer_message:action.body.message[0],
          type:action.type
        }
      }

      case "SETAUCTIONBID_PENDING":{
        return{
          ...state,
          type:action.type
        }
    }
    case "SETAUCTIONBID_SUCCESS":{
        return{
          ...state,
          auction_bid_message:action.body.message[0],
          auction_bid_status_code:action.body.status_code,
          auction_bid: action.body,
          type:action.type
        }
    }
    case "SETAUCTIONBID_FAILD":{
      return{
        ...state,
        reducer_message:action.error,
        type:action.type
      }
    }

    case "GETCURRENTBIDAMOUNT_PENDING":{
      return{
        ...state,
        type:action.type
      }
  }
  case "GETCURRENTBIDAMOUNT_SUCCESS":{
      return{
        ...state,
        current_bid_price:action.body.data,
        type:action.type
      }
  }
  case "GETCURRENTBIDAMOUNT_FAILD":{
    return{
      ...state,
      reducer_message:action.error,
      type:action.type
    }
  }

  case "GETCURRENTPROXYBIDAMOUNT_PENDING":{
    return{
      ...state,
      type:action.type
    }
}
case "GETCURRENTPROXYBIDAMOUNT_SUCCESS":{
    return{
      ...state,
      current_proxy_bid_price:action.body,
      type:action.type
    }
}
case "GETCURRENTPROXYBIDAMOUNT_FAILD":{
  return{
    ...state,
    reducer_message:action.error,
    type:action.type
  }
}

case "GETHISTORYBID_PENDING":{
  return{
    ...state,
    type:action.type
  }
}
case "GETHISTORYBID_SUCCESS":{
  return{
    ...state,
    history_bid_data:action.body,
    type:action.type
  }
}
case "GETHISTORYBID_FAILD":{
return{
  ...state,
  reducer_message:action.error,
  type:action.type
}
}

case "SETWISTLIST_PENDING":{
  return{
    ...state,
    type:action.type
  }
}
case "SETWISTLIST_SUCCESS":{
  return{
    ...state,
    add_to_wish_list:action.body,
    type:action.type
  }
}
case "SETWISTLIST_FAILD":{
return{
  ...state,
  reducer_message:action.error,
  type:action.type
}
}

case "GETALLWISHLISTITEM_PENDING":{
  return{
    ...state,
    type:action.type
  }
}
case "GETALLWISHLISTITEM_SUCCESS":{
  return{
    ...state,
    get_all_wish_list_item:action.body,
    type:action.type
  }
}
case "GETALLWISHLISTITEM_FAILD":{
return{
  ...state,
  reducer_message:action.error,
  type:action.type
}
}

case "DEPOSITCHECKOUT_PENDING":{
  return{
    ...state,
    type:action.type
  }
}
case "DEPOSITCHECKOUT_SUCCESS":{
  return{
    ...state,
    deposit_checkout: action.body.data,
    status_code:action.body.status_code,
    type:action.type
  }
}

case "DEPOSITCHECKOUT_FAILD":{
    return{
      ...state,
      reducer_message:action.body.message,
      status_code:action.body.status_code,
      type:action.type
    }
}

case "SUBMITDEPOSITCHECKOUT_PENDING":{
  return{
    ...state,
    type:action.type
  }
}

case "SUBMITDEPOSITCHECKOUT_SUCCESS":{
  return{
    ...state,
    submit_deposit_checkout:action.body.data,
    deposit_status_code:action.body.status_code,
    type:action.type
  }
}
case "SUBMITDEPOSITCHECKOUT_FAILD":{
  return{
    ...state,
    reducer_message:action.body.message[0],
    deposit_status_code:action.body.status_code,
    type:action.type
  }
}

case "AUCTIONSELLERQUESTION_PENDING":{
  return{
    ...state,
    type:action.type
  }  
}
case "AUCTIONSELLERQUESTION_SUCCESS":{
  return{
    ...state,
    auctionQuestion:action.body,
    type:action.type
  }
}

case "AUCTIONSELLERQUESTION_FAILD":{
  return{
    ...state,
    reducer_message:action.body.message[0],
    type:action.type
  }
}

case "AUCTIONCHECKOUT_PENDING":{
  return{
    ...state,
    type:action.type
  }  
}
case "AUCTIONCHECKOUT_SUCCESS":{
  return{
    ...state,
    auctionCheckout:action.body,
    auctionCheckout_data:action.body.data,
    type:action.type
  }
}

case "AUCTIONCHECKOUT_FAILD":{
  return{
    ...state,
    reducer_message:action.body.message[0],
    type:action.type
  }
}

 case "BUYNOWCHECKOUT_PENDING":{
      return {
        ...state,
        type:action.type
      }
    }

    case "BUYNOWCHECKOUT_SUCCESS":{
      return {
        ...state,
        buynow_checkout_data: action.body,
        buynow_checkout_data_all:action.body.data,
        type:action.type
      }
    }

    case "BUYNOWCHECKOUT_FAILD":{
      return {
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETITEMSHIPPING_PENDING": {
      return {
        ...state,
        type: action.type
      }
    }

    case "GETITEMSHIPPING_SUCCESS": {
      return {
        ...state,
        get_item_shipping: action.body,
        type: action.type
      }
    }

    case "GETITEMSHIPPING_FAILD": {
      return {
        ...state,
        reducer_message: action.body.message[0],
        type: action.type
      }
    }

    case "BUYERADDRESSLIST_PENDING": {
      return {
        ...state,
        type:action.type
      }
    }

    case "BUYERADDRESSLIST_SUCCESS": {
      return {
        ...state,
        addresslist_data:action.body,
        type:action.type
      }
    }

    case "BUYERADDRESSLIST_FAILD": {
      return {
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "DELETENOWQUOT_PENDING": {
      return {
        ...state,
        type: action.type
      }
    }

    case "DELETENOWQUOT_SUCCESS": {
      return {
        ...state,
        close_envelope_bid: action.body,
        type: action.type
      }
    }

    case "DELETENOWQUOT_FAILD": {
      return {
        ...state,
        reducer_message: action.message[0],
        type: action.type
      }
    }

    case "BUYNOWORDER_PENDING" : { 
      return{
        ...state,
        type:action.type
      }
    }

    case "BUYNOWORDER_SUCCESS" : {
      return{
        ...state,
        buynow_order:action.body,
        type:action.type
      }
    }

    case "BUYNOWORDER_FAILD": {
      return{
        ...state,
        reducer_message:action.body.message[0],
        buynow_order_faild: action.body.status_code,
        type:action.type
      }
    }

    case "DELETE_QUEUEITEM_PENDING" : { 
      return{
        ...state,
        type:action.type
      }
    }

    case "DELETE_QUEUEITEM_SUCCESS" : {
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "DELETE_QUEUEITEM_FAILD": {
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    


    case "GETCOUNTRY_PENDING" : { 
      return{
        ...state,
        type:action.type
      }
    }

    case "GETCOUNTRY_SUCCESS" : {
      return{
        ...state,
        get_country:action.body,
        type:action.type
      }
    }

    case "GETCOUNTRY_FAILD": {
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETSTATE_PENDING" : { 
      return{
        ...state,
        type:action.type
      }
    }

    case "GETSTATE_SUCCESS" : {
      return{
        ...state,
        get_state:action.body,
        type:action.type
      }
    }

    case "GETSTATE_FAILD": {
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }


    case "GETCITY_PENDING" : { 
      return{
        ...state,
        type:action.type
      }
    }

    case "GETCITY_SUCCESS" : {
      return{
        ...state,
        get_city:action.body,
        type:action.type
      }
    }

    case "GETCITY_FAILD": {
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }


    case "GETISD_PENDING" : { 
      return{
        ...state,
        type:action.type
      }
    }

    case "GETISD_SUCCESS" : {
      return{
        ...state,
        get_isd:action.body,
        type:action.type
      }
    }

    case "GETISD_FAILD": {
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "SUBMIT_CLOSE_ENVELOPE_PENDING":{
      return{
        ...state,
        reducer_message:'pending',
        type:action.type
      }
    }

    case "SUBMIT_CLOSE_ENVELOPE_SUCCESS":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        submit_close_en:action.body,
        type:action.type
      }
    }

    case "SUBMIT_CLOSE_ENVELOPE_FAILD":{
      return{
        ...state,
        reducer_message:'something went wrong',//action.body.message,
        //submit_close_en:action.body,
        type:action.type
      }
    }

    case "GET_PRIVATE_AUCTION_LIST_PENDING":{
      return{
        ...state,
        reducer_message:'pending',
        type:action.type
      }
    }

    case "GET_PRIVATE_AUCTION_LIST_SUCCESS":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        get_private_auction:action.body,
        type:action.type
      }
    }

    case "GET_PRIVATE_AUCTION_LIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }


    case "GETBUYERADDRESSLIST_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETBUYERADDRESSLIST_SUCCESS":{
      return{
        ...state,
        get_buyer_address_list_data:action.body,
        type:action.type
      }
    }

    case "GETBUYERADDRESSLIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }


    case "ADDBUYERADDRESS_PENDING":{
      return{
        ...state,
        reducer_message:'pending',
        type:action.type
      }
    }

    case "ADDBUYERADDRESS_SUCCESS":{
      return{
        ...state,
        set_buyer_address_data:action.body,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "ADDBUYERADDRESS_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "SETPRIMARYADDRESS_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "SETPRIMARYADDRESS_SUCCESS":{
      return{
        ...state,
        set_primary_address:action.body,
        type:action.type
      }
    }

    case "SETPRIMARYADDRESS_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "DELETEBUYERADDRESS_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "DELETEBUYERADDRESS_SUCCESS":{
      return{
        ...state,
        delete_buyer_address:action.body,
        type:action.type
      }
    }

    case "DELETEBUYERADDRESS_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETCREDITCARDLIST_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETCREDITCARDLIST_SUCCESS":{
      return{
        ...state,
        get_all_credit_card:action.body.data,
        status_code:action.body.status_code,
        type:action.type
      }
    }

    case "GETCREDITCARDLIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        status_code:action.body.status_code,
        type:action.type
      }
    }

    case "SETPRIMARYCARD_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "SETPRIMARYCARD_SUCCESS":{
      return{
        ...state,
        set_primary_credit_card:action.body,
        type:action.type
      }
    }

    case "SETPRIMARYCARD_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "DELETEBUYERCREDITCARD_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "DELETEBUYERCREDITCARD_SUCCESS":{
      return{
        ...state,
        delete_buyer_credit_card:action.body,
        type:action.type
      }
    }

    case "DELETEBUYERCREDITCARD_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "ADDCREDITCARDDATA_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "ADDCREDITCARDDATA_SUCCESS":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "ADDCREDITCARDDATA_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "EDITBUYERADDRESS_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "EDITBUYERADDRESS_SUCCESS":{
      return{
        ...state,
        edit_buyer_address:action.body.message[0],
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "EDITBUYERADDRESS_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETINVOICELIST_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETINVOICELIST_SUCCESS":{
      return{
        ...state,
        buyer_invoice:action.body,
        type:action.type
      }
    }

    case "GETINVOICELIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }
    case "GETINVOICEDETAIL_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETINVOICEDETAIL_SUCCESS":{
      return{
        ...state,
        buyer_invoice_detail:action.body,
        type:action.type
      }
    }

    case "GETINVOICEDETAIL_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETWONLIST_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETWONLIST_SUCCESS":{
      return{
        ...state,
        won_list:action.body.data,
        type:action.type
      }
    }

    case "GETWONLIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETNOTWON_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETNOTWON_SUCCESS":{
      return{
        ...state,
        not_won_list:action.body.data,
        type:action.type
      }
    }

    case "GETNOTWON_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }
    case "GETACTIVELIST_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETACTIVELIST_SUCCESS":{
      return{
        ...state,
        active_list:action.body.data,
        type:action.type
      }
    }

    case "GETACTIVELIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETFEEDBACK_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETFEEDBACK_SUCCESS":{
      return{
        ...state,
        get_feedack_list:action.body.data.feedback_data,
        type:action.type
      }
    }

    case "GETFEEDBACK_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "TRYCATCH_FAILD":{
      return{
        ...state,
        type:action.type
      }
    }

    case "EDITPROFILE_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "EDITPROFILE_SUCCESS":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "EDITPROFILE_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "CHANGEPASSWORD_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "CHANGEPASSWORD_SUCCESS":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "CHANGEPASSWORD_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "FORGOTPASSWORD_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "FORGOTPASSWORD_SUCCESS":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "FORGOTPASSWORD_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "ADD_PAYMENT_WALLET_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "ADD_PAYMENT_WALLET_SUCCESS":{
      return{
        ...state,
        add_payment_wallet:action.body,
        type:action.type
      }
    }
    
    case "ADD_PAYMENT_WALLET_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }


    case "GET_PAYMENT_WALLET_HISTORY_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GET_PAYMENT_WALLET_HISTORY_SUCCESS":{
      return{
        ...state,
        get_payment_wallet_history_data:action.body.data.wallet_data,
        type:action.type
      }
    }

    case "GETDASHBOARD_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETDASHBOARD_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETDASHBOARD_SUCCESS":{
      return{
        ...state,
        get_dashboard:action.body,
        type:action.type
      }
    }

    case "GET_PAYMENT_WALLET_HISTORY_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETPROFILE_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETPROFILE_SUCCESS":{
      return{
        ...state,
        buyerProfile:action.body.data[0],
        type:action.type
      }
    }

    case "GETPROFILE_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETBUYNOWLIST_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETBUYNOWLIST_SUCCESS":{
      return{
        ...state,
        buynowlist:/* action.body.data */action.body.data.length !== 0 ? action.body.data.product_data : [],
        type:action.type
      }
    }

    case "GETBUYNOWLIST_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "SETSELLERFEEDBACK_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "SETSELLERFEEDBACK_SUCCESS":{
      return{
        ...state,
        sellerfeedback:action.body,
        type:action.type
      }
    }

    case "SETSELLERFEEDBACK_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }
    case "SETSELLERCONTACT_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "SETSELLERCONTACT_SUCCESS":{
      return{
        ...state,
        sellercontact:action.body,
        type:action.type
      }
    }

    case "SETSELLERCONTACT_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }
    
    case "SOMETHING_WENT_WRONG":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETALLLABELS_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETALLLABELS_SUCCESS":{
      console.log(JSON.stringify(action.body.data['en']))
      return{
        ...state,
        allLabels:action.body.data,
        type:action.type
      }
    }

    case "GETALLLABELS_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }

    case "GETCETOGORYLISTINTERVAL_PENDING":{
      return{
        ...state,
        type:action.type
      }
    }

    case "GETCETOGORYLISTINTERVAL_SUCCESS":{
      console.log(JSON.stringify(action.body.data['en']))
      return{
        ...state,
        categorylistinterval:action.body.data,
        type:action.type
      }
    }

    case "GETCETOGORYLISTINTERVAL_FAILD":{
      return{
        ...state,
        reducer_message:action.body.message[0],
        type:action.type
      }
    }
    
    default:
      return state;
  }
 
}










