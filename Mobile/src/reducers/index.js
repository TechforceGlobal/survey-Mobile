import {combineReducers} from 'redux';
import ReducerStore from './ReducerStore';
import PaymentHistoryReducer from './PaymentHistoryReducer';
import LanguageReducer from './LanguageReducer';
const allReducers= combineReducers({
  ReducerStore,
  LanguageReducer,
  PaymentHistoryReducer
});
export default allReducers;