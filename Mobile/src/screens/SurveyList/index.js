import React, { Component } from 'react';
import { View, Text, Image, Alert, TextInput, ScrollView, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';
import { styles } from './styles';
import { Header, Breadcumb, Loader, MyStatusbar, Survey_Loader } from '../../components';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';
import { api_url } from '../../common'
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });

var routes = ['Home']
export class SurveyList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      emailError: false,
      passwordError: false,
      showHide_passord: true,
      menuhandledata: [],
      bodyArray: [],
      token: '',
      loading: false,
      surveyoremail: '',
      loadingText: 'Loading...'
    }
  }

  async componentDidMount() {
    const { surveyoremail } = this.props.navigation.state.params;
    let survey = [];
    let menuhandledata = [];

    let CompletedSurvey = [];
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM CompletedSurvey',
        [],
        (tx, results) => {

          for (let k = 0; k < results.rows.length; k++) {
            CompletedSurvey.push(results.rows.item(k));
          }
          console.log('CompletedSurvey: ' + JSON.stringify(CompletedSurvey));
          global.savedRes = CompletedSurvey.length
        }
      )
    })

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM Survey',
        [],
        (tx, results) => {

          for (let k = 0; k < results.rows.length; k++) {
            survey.push(results.rows.item(k));
          }
          console.log('survey: ' + JSON.stringify(survey))

          if (survey.length !== 0) {
            for (let i = 0; i < survey.length; i++) {
              let opensubMenuObj = { opensubMenu: false }
              menuhandledata.push(opensubMenuObj)
            }
            this.setState({
              bodyArray: survey,
              menuhandledata,
              loading: false,
              surveyoremail
            })
          } else {
            this.loadAllData()
          }
        }
      );
    });
  }

  async loadAllData() {
    await this.getData()
    await this.call_api_surveyApp()
    await this.LoadCompaniesList();
    await this.loadSurveyMainData();
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key')
      if (value !== null) {
        this.setState({
          token: value
        })
        console.log('Value from asynce  ' + JSON.parse(JSON.stringify(this.state.token)))
      }
    } catch (e) {
      console.log('erro from Surveylist asynce' + JSON.stringify(e))
    }
  }
  call_api_surveyApp = () => {
    this.setState({
      loading: true
    })
    let url = api_url + 'surveygrouplist'
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'token': `${this.state.token}`
      }
    }
    fetch(url, options).then(async (response) =>
      response.json().then((body) => {
        let dummyArray = [];
        for (let i = 0; i < body.length; i++) {
          dummyArray.push(body[i])
        }

        var survey = [];
        var all_CompanyIds = [];
        var all_locationIds = [];
        for (let i = 0; i < body.length; i++) {
          var CompanyIds = [];
          var locationIds = [];
          for (let j = 0; j < body[i].subsurvey.length; j++) {
            var company_id = body[i].subsurvey[j].company_id;
            CompanyIds.push(company_id);
            var location_id = body[i].subsurvey[j].location_id;
            locationIds.push(location_id);
          }
          console.log('CompanyIds: ' + JSON.stringify(CompanyIds))
          console.log('locationIds: ' + JSON.stringify(locationIds))
          all_CompanyIds.push(CompanyIds);
          all_locationIds.push(locationIds)
        }

        for (let i = 0; i < body.length; i++) {
          db.transaction(tx => {
            tx.executeSql(
              'INSERT INTO Survey (id,survey,CompanyIds,locationIds) VALUES (?,?,?,?)',
              [parseInt(body[i].id), `${body[i].survey}`, all_CompanyIds[i].toString(), all_locationIds[i].toString()],
              (tx, results) => {
                //console.log('============ Database operations1 ============')
                console.log('results: ' + JSON.stringify(results))
                if (i === body.length - 1) {
                  db.transaction(tx => {
                    tx.executeSql(
                      'SELECT * FROM Survey',
                      [],
                      (tx, results) => {
                        for (let j = 0; j < results.rows.length; j++) {
                          survey.push(results.rows.item(j));
                        }
                        this.setState({ bodyArray: survey })
                        //console.log('Survey: ' + JSON.stringify(survey))
                        let SubSurvey = []
                        for (let k = 0; k < body.length; k++) {
                          for (let n = 0; n < body[k].subsurvey.length; n++) {
                            db.transaction(tx => {
                              tx.executeSql(
                                'INSERT INTO SubSurvey (survey_id,subsurvey_id,subsurveyTitle,companyId,locationId) VALUES (?,?,?,?,?)',
                                [parseInt(body[k].id), parseInt(body[k].subsurvey[n].id), body[k].subsurvey[n].name, parseInt(body[k].subsurvey[n].company_id), parseInt(body[k].subsurvey[n].location_id)],
                                (tx, results) => {
                                  if (k === body.length - 1) {
                                    db.transaction(tx => {
                                      tx.executeSql(
                                        'SELECT * FROM SubSurvey',
                                        [],
                                        (tx, results) => {
                                          for (let j = 0; j < results.rows.length; j++) {
                                            SubSurvey.push(results.rows.item(j));
                                          }
                                          //console.log('SubSurvey: ' + JSON.stringify(SubSurvey))

                                        }
                                      );
                                    });
                                  }

                                }
                              );
                            });

                          }
                        }
                      }
                    );
                  });
                }
              }
            );
          });
        }

      }).catch((error) =>
        this.setState({
          loading: false
        }, () => {
          console.log("API ERROR_RESPONSE: ", JSON.stringify(error))
        })
      )
    ).catch((error) => {
      this.setState({
        loading: false
      }, () => {
        console.log("API ERROR_OUTER: ", JSON.stringify(error));
        this.setState({ loading: false }, () => {
          Alert.alert(
            '',
            'Oops, Something went wrong',
            [
              { text: 'OK', onPress: () => this.props.navigation.navigate('Login') },
            ],
            { cancelable: false }
          )
        })
      })
    })
  }

  LoadCompaniesList = () => {
    let url = api_url + 'companylist'
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'token': this.state.token
      }
    }

    fetch(url, options).then((response) => {
      response.json().then((body) => {
        let company_api_array = [];
        for (let i = 0; i < body.length; i++) {
          company_api_array.push(body[i])
        }
        //console.log('company_api_array: ' + JSON.stringify(company_api_array))

        var Company = [];
        var CompanyLocation = [];
        for (let i = 0; i < body.length; i++) {

          db.transaction(tx => {
            tx.executeSql(
              'INSERT INTO Company (id, name) VALUES (?,?)',
              [parseInt(body[i].id), `${body[i].name}`],
              (tx, results) => {

                for (let j = 0; j < body[i].locations.length; j++) {
                  db.transaction(tx => {
                    tx.executeSql(
                      'INSERT INTO CompanyLocation (id,CompanyId,name,headcount,completedcount) VALUES (?,?,?,?,?)',
                      [parseInt(body[i].locations[j].id), parseInt(body[i].id), `${body[i].locations[j].name}`,parseInt(body[i].locations[j].Headcount),0],
                      (tx, results) => {

                      }
                    );
                  });
                }

                if (i === body.length - 1) {
                  db.transaction(tx => {
                    tx.executeSql(
                      'SELECT * FROM Company',
                      [],
                      (tx, results) => {

                        for (let j = 0; j < results.rows.length; j++) {
                          Company.push(results.rows.item(j));
                        }
                        //console.log('Company: ' + JSON.stringify(Company))

                        db.transaction(tx => {
                          tx.executeSql(
                            'SELECT * FROM CompanyLocation',
                            [],
                            (tx, results) => {

                              for (let k = 0; k < results.rows.length; k++) {
                                CompanyLocation.push(results.rows.item(k));
                              }
                            }
                          );
                        });
                      }
                    );
                  });
                }

              }
            );
          });
        }
      }).catch((error) => {
        console.log('Inner Error=>' + JSON.stringify(error))
      })
    }).catch((error) => {
      console.log('outer Error=>' + JSON.stringify(error))
      this.setState({ loading: false }, () => {
        Alert.alert(
          '',
          'Oops, Something went wrong',
          [
            { text: 'OK', onPress: () => this.props.navigation.navigate('Login') },
          ],
          { cancelable: false }
        )
      })
    })
  }

  loadSurveyMainData = () => {
    let url = api_url + 'surveylist'
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'token': this.state.token
      }
    }

    let Language = [];
    fetch(url, options).then((response) => {
      response.json().then(async (body) => {
        let resource = body.resource
        let resource_original = []
        let resource_donwloaded = []

        for (let r = 0; r < resource.length; r++){
          let obj = {'key':r,'img':resource[r]};
          resource_original.push(obj)
        }

        Promise.all(resource_original.map((image) =>
          RNFetchBlob
            .config({
              fileCache: true,
            })
            .fetch('GET', image.img, {
            })
            .then((res) => {
              console.log('The assets saved to ' + image.key + ' , ' + res.path())
              //console.log('QUE LOG: ' + 'orderId: ' + orderId + 'langCode: ' + question_data_languageKey[k] + 'surveyId: ' + parseInt(body[Q].survey_id) + 'body[Q].subsurveycompany: ' + body[Q].subsurveycompany + 'body[Q].subsurveylocation: ' + body[Q].subsurveylocation + 'Que: ' + question_data_languageWise[question_data_languageKey[k]]['question'] + 'Option: ' + (question_data_languageWise[question_data_languageKey[k]]['option']).toString())
              this.setState({loadingText:`Downloading image ${image.key} of ${resource_original.length}`})
              let obj = {'key':image.key,'img':res.path()}
              resource_donwloaded.push(obj)
            })//Rn-fetch-blob closing#
        ))
          .then(onComplete => {
            console.log('The assets obj ' + JSON.stringify(resource_donwloaded))

            for (let r = 0; r < resource.length; r++) {
              this.setState({loadingText:`Adding image ${r} of ${resource.length} to database`})
             let imageToInsert = ''
             for (let f = 0; f < resource_donwloaded.length; f++){
               if(resource_donwloaded[f].key === r){
                imageToInsert = resource_donwloaded[f].img
               }
             }
              db.transaction(tx => {
                tx.executeSql(
                  'INSERT INTO assets (image) VALUES (?)',
                  [
                    imageToInsert
                  ],
                  (tx, resultsR) => {
                    if (r === resource.length - 1) {
                      console.log('All images downloaded...')
                    }
                  }
                );
              });
            }

            let survey_list = body.survey_list
            for (let i = 0; i < survey_list.length; i++) {
              this.setState({loadingText:`Adding survey(s) to database`})
              let lang_keys = Object.keys(survey_list[i].subsurveylanguage);
              let lang_values = Object.values(survey_list[i].subsurveylanguage);
              db.transaction(tx => {
                tx.executeSql(
                  'INSERT INTO Language (surveyId,companyId,locationId,language_codes,language_values) VALUES (?,?,?,?,?)',
                  [parseInt(survey_list[i].survey_id), survey_list[i].subsurveycompany, survey_list[i].subsurveylocation, (lang_keys).toString(), (lang_values).toString()],
                  (tx, results) => {

                    if (i === survey_list.length - 1) {
                      db.transaction(tx => {
                        tx.executeSql(
                          'SELECT * FROM Language',
                          [],
                          (tx, results) => {

                            for (let j = 0; j < results.rows.length; j++) {
                              Language.push(results.rows.item(j));
                            }

                            /* Adding Question Data...*/
                            for (let Q = 0; Q < survey_list.length; Q++) {
                              var questionsArray = survey_list[Q].question
                              for (let j = 0; j < questionsArray.length; j++) {
                                let question_id = questionsArray[j].question_id
                                console.log('question_id ' + question_id)
                                let orderId = parseInt(questionsArray[j].id)
                                console.log('orderId ' + orderId)
                                let type = questionsArray[j].type
                                console.log('type ' + type)
                                let img_src = questionsArray[j].img_src
                                console.log('img_src ' + img_src)
                                let question_data_languageKey = Object.keys(questionsArray[j].questions_data.languages)
                                console.log('question_data_languageKey ' + JSON.stringify(question_data_languageKey))
                                let question_data_languageWise = questionsArray[j].questions_data.languages
                                console.log('question_data_languageWise ' + JSON.stringify(question_data_languageWise))

                                for (let k = 0; k < question_data_languageKey.length; k++) {
                                  console.log('The que image saved to ' + Q + ' , ' + j + ', ' + k)
                                  db.transaction(tx => {
                                    tx.executeSql(
                                      'INSERT INTO Question (orderId,quetion_id,lang_code,surveyId,companyId,locationId,Question,type,Options,image) VALUES (?,?,?,?,?,?,?,?,?,?)',
                                      [
                                        orderId,
                                        `${question_id}`,
                                        question_data_languageKey[k],
                                        parseInt(survey_list[Q].survey_id),
                                        survey_list[Q].subsurveycompany,
                                        survey_list[Q].subsurveylocation,
                                        question_data_languageWise[question_data_languageKey[k]]['question'],
                                        type,
                                        (question_data_languageWise[question_data_languageKey[k]]['option']).toString(),
                                        img_src
                                      ],
                                      (tx, results0) => {
                                        console.log('survey_list.length: ' + survey_list.length + ' ,questionsArray.length: ' + questionsArray.length + ',question_data_languageKey.length: ' + question_data_languageKey.length)
                                        if (Q === survey_list.length - 1 && j === questionsArray.length - 1 && k === question_data_languageKey.length - 1) {
                                          this.setState({
                                            loading: false
                                          })
                                        }
                                      }
                                    );
                                  });

                                }
                              }

                            }

                            /* Closing Adding of Question Data### */
                            //console.log('Language: ' + JSON.stringify(results.rows.item(0))[Languages])
                          }
                        );
                      });
                    }

                  }
                );
              });

              //Common value
              var myLangs = Object.keys(survey_list[i].subsurveylanguagedata);
              var obj = survey_list[i].subsurveylanguagedata
              //Gives languages ['ar','en'.'hi']
              let common = []
              for (let j = 0; j < myLangs.length; j++) {
                let Welcometext = obj[myLangs[j]]["Welcometext"]
                let thankyoutext = obj[myLangs[j]]["thankyoutext"]
                let changelanguage = obj[myLangs[j]]["changelanguage"]
                let startsurvey = obj[myLangs[j]]["startsurvey"]
                let previous = obj[myLangs[j]]["previous"]
                db.transaction(tx => {
                  tx.executeSql(
                    'INSERT INTO common (expiryDate,surveyId,companyId,locationId,lang_code,Welcometext,thankyoutext,changelanguage,startsurvey,previous,header_src,footer_src,footer2_src) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',
                    [
                      survey_list[i].enddate,
                      parseInt(survey_list[i].survey_id), survey_list[i].subsurveycompany, survey_list[i].subsurveylocation,
                      myLangs[j],
                      Welcometext,
                      thankyoutext,
                      changelanguage,
                      startsurvey,
                      previous,
                      survey_list[i].header_src,
                      survey_list[i].footer_src,
                      survey_list[i].footer2_src
                    ],
                    (tx, results) => {
                      if (i === survey_list.length - 1) {
                        db.transaction(tx => {
                          tx.executeSql(
                            'SELECT * FROM common',
                            [],
                            (tx, results) => {
                              for (let j = 0; j < results.rows.length; j++) {
                                common.push(results.rows.item(j));
                              }
                            }
                          );
                        });
                      }

                    }
                  );
                });
              }
            }

          })
      }).catch((error) => {
        console.log('Inner Error=>' + JSON.stringify(error))
      })
    }).catch((error) => {
      console.log('outer Error=>' + JSON.stringify(error))
      this.setState({ loading: false }, () => {
        Alert.alert(
          '',
          'Oops, Something went wrong',
          [
            { text: 'OK', onPress: () => this.props.navigation.navigate('Login') },
          ],
          { cancelable: false }
        )
      })
    })
  }

  navigateToCopanyList = (surveyItem) => {
    console.log('surveyItem: ' + JSON.stringify(surveyItem))
    this.props.navigation.navigate('CompanyList', { surveyItem, surveyoremail: this.state.surveyoremail })
    /*  for (let i = 0; i < this.state.bodyArray.length; i++) {
       let menuhandledata = this.state.menuhandledata;
       menuhandledata[i].openSubmenu = false;
       this.setState({ menuhandledata })
     } */
  }

  openMenu = (index) => {
    let menuhandledata = this.state.menuhandledata;
    menuhandledata[index].openSubmenu = !menuhandledata[index].openSubmenu;
    this.setState({ menuhandledata })
  }

  onSurveyPressed = (item, index) => {
    let Subsurvey = []
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM SubSurvey WHERE SurveyId = ?',
        [item.id],
        (tx, results) => {

          for (let k = 0; k < results.rows.length; k++) {
            Subsurvey.push(results.rows.item(k));
          }
          let bodyArray = this.state.bodyArray;
          bodyArray[index].subsurvey = Subsurvey;
          if (Subsurvey.length === 0) {
            this.navigateToCopanyList(item.survey, item.CompanyIds)
          } else {
            this.setState({ bodyArray }, () => {
              this.openMenu(index)
            })
          }
        }
      );
    });
  }

  render() {
    const { menuhandledata } = this.state;
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName="Available Groups" />
        <Breadcumb routes={routes} />
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
          <View style={styles.listStyle}>
            {
              this.state.bodyArray.map((item, index) => {
                return (
                  <View>
                    <TouchableOpacity style={styles.listItemStyle} onPress={() => this.navigateToCopanyList(item)}>
                      <Text style={styles.listItemTextStyle}>{item.survey}</Text>
                    </TouchableOpacity>
                  </View>
                )
              })
            }
          </View>
        </ScrollView>
        {
          this.state.loading &&
          <Survey_Loader loadingText={this.state.loadingText} />
        }
      </ImageBackground>

    )
  }
}

export default SurveyList;