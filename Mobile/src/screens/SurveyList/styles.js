import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width
export const styles = StyleSheet.create({
    scrollViewStyle: {
        flexGrow: 1,
    },
    listStyle:{
        marginHorizontal:12,
        marginVertical:15,
        borderTopWidth:1
    },
    listItemStyle:{
        flexDirection:'row',
        backgroundColor:'#d1d2d4',
        borderBottomWidth:1,
        paddingVertical:12,
        paddingHorizontal:12,
        borderLeftWidth:1,
        borderRightWidth:1,
        borderLeftColor:'#8f9192',
        borderRightColor:'#8f9192',
        alignItems:'center'
    },
    listItemTextStyle:{
        flex:1,
        color:'#155f90',
        fontSize:15,
        fontFamily:'Poppins-Regular'
    },
    subItemStyle:{
        flexDirection:'row',
        backgroundColor:'#fff',
        borderBottomWidth:1,
        paddingVertical:12,
        paddingHorizontal:12,
        borderLeftWidth:1,
        borderRightWidth:1,
        borderLeftColor:'#8f9192',
        borderRightColor:'#8f9192',
        alignItems:'center'
    },
    subItemTextStyle:{
        flex:1,
        color:'#404041',
        fontSize:15,
        fontFamily:'Poppins-Regular'
    },
    textboxIconStyle:{
        height:15,
        width:15,
        resizeMode:'contain'
    },
})