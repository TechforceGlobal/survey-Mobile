import React, { Component } from 'react';
import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";

var Aes = NativeModules.Aes;
const generateKey = (password, salt, cost, length) => Aes.pbkdf2(password, salt, cost, length);

const encrypt = (text, key) => {
  return Aes.randomKey(16).then(iv => {
    return Aes.encrypt(text, key, iv).then(cipher => ({
      cipher,
      iv,
    }))
  })
};

const decrypt = (encryptedData, key) => Aes.decrypt(encryptedData.cipher, key, encryptedData.iv);

export class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
    }
  }

  componentDidMount() {
    this.encryptKey()
    //this.setTitle()
  }

  setTitle = () => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        this.props.navigation.navigate('No connection');
      } else {
        this.setState({ title: 'Welcome to the Survey App.' }, () => {
          setTimeout(() => {
            this.setState({ title: 'Survey App' })
          }, 3000);
        })
      }
    })

  }

  encryptKey = () => {
    let plainText = '{"email":"chirag.shah@techforceglobal.com","password":"Survey@2020"}';
    let password = 'Survey@2020'
    let sha256_result = ''
    let sha_256_32str = ''
    try {
      /* Aes.sha256(plainText).then((plainText)=>{
        //console.log('Encrypted:' + encryptedText)
      }) */
      Aes.sha256(password).then(sha256 => {
        sha256_result = sha256;
        sha_256_32str = sha256_result.substring(0, 32);
        console.log('sha256: ' + sha256);

        try {
          generateKey('survey_idg', sha_256_32str, 5000, 256).then(key => {
            console.log('Key:', key)
            encrypt(plainText, key)
              .then(({ cipher, iv }) => {
                console.log('Encrypted:', cipher)
    
                decrypt({ cipher, iv }, key)
                  .then(text => {
                    console.log('Decrypted:', text)
                  })
                  .catch(error => {
                    console.log(error)
                  })
    
                Aes.hmac256(cipher, key).then(hash => {
                  console.log('HMAC', hash)
                })
              })
              .catch(error => {
                console.log(error)
              })
          })
        } catch (e) {
          console.error(e)
        }
      }
      )
    }
    catch (error) {
      console.log(error)
    }


    /* try {
      generateKey(plainText, 'survey_idg', 5000, 256).then(key => {
        console.log('Key:', key)
        encrypt('These violent delights have violent ends', key)
          .then(({ cipher, iv }) => {
            console.log('Encrypted:', cipher)

            decrypt({ cipher, iv }, key)
              .then(text => {
                console.log('Decrypted:', text)
              })
              .catch(error => {
                console.log(error)
              })

            Aes.hmac256(cipher, key).then(hash => {
              console.log('HMAC', hash)
            })
          })
          .catch(error => {
            console.log(error)
          })
      })
    } catch (e) {
      console.error(e)
    } */
  }

  checkConnectivity = () => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        this.props.navigation.navigate('No connection');
      }
    })
  }

  render() {
    const { title } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {/* <NavigationEvents
          onDidFocus={payload => this.setTitle()}
        /> */}
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity style={styles.buttonStyle} onPress={this.checkConnectivity}>
          <Text style={styles.buttonText}>Check Internet</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    margin: 12,
    fontSize: 16,
    fontWeight: '700',
    flex: 1,
    textAlign: 'center'
  },
  buttonStyle: {
    width: '80%',
    backgroundColor: '#4285f4',
    borderRadius: 4,
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignSelf: 'center',
    marginBottom: 10
  },
  buttonText: {
    color: '#fff',
    fontWeight: '500',
    textAlign: 'center',
    fontSize: 15
  }
})

export default HomeScreen;