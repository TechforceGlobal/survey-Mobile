import React, { Component } from 'react';
import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";

var Aes = NativeModules.Aes;
const generateKey = (password, salt, cost, length) => Aes.pbkdf2(password, salt, cost, length);

const encrypt = (text, key) => {
  return Aes.randomKey(16).then(iv => {
    return Aes.encrypt(text, key, iv).then(cipher => ({
      cipher,
      iv,
    }))
  })
};

const decrypt = (encryptedData, key) => Aes.decrypt(encryptedData.cipher, key, encryptedData.iv);

export class NoConnectivity extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
    }
  }

  componentDidMount() {
    //this.encryptKey()
    this.setTitle()
  }

  setTitle = () => {
    this.setState({ title: 'Welcome to the Survey App.' }, () => {
      setTimeout(() => {
        this.setState({ title: 'Survey App' })
      }, 3000);
    })
  }

  encryptKey = () => {
    try {
      generateKey('Arnold', 'salt', 5000, 256).then(key => {
        console.log('Key:', key)
        encrypt('These violent delights have violent ends', key)
          .then(({ cipher, iv }) => {
            console.log('Encrypted:', cipher)

            decrypt({ cipher, iv }, key)
              .then(text => {
                console.log('Decrypted:', text)
              })
              .catch(error => {
                console.log(error)
              })

            Aes.hmac256(cipher, key).then(hash => {
              console.log('HMAC', hash)
            })
          })
          .catch(error => {
            console.log(error)
          })
      })
    } catch (e) {
      console.error(e)
    }
  }

  checkConnectivity = () => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (isConnected) {
        this.props.navigation.goBack()
      }
    })
  }

  render() {
    return (<View style={{ flex: 1 }}>
          <Text style={styles.title}>Oops! It seems you lost internet connectivity</Text>
          <TouchableOpacity style={styles.buttonStyle} onPress={this.checkConnectivity}>
            <Text style={styles.buttonText}>Refresh</Text>
          </TouchableOpacity>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    margin: 12,
    fontSize: 16,
    fontWeight: '700',
    flex: 1,
    textAlign: 'center'
  },
  buttonStyle: {
    width: '80%',
    backgroundColor: '#4285f4',
    borderRadius: 4,
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignSelf: 'center',
    marginBottom: 10
  },
  buttonText: {
    color: '#fff',
    fontWeight: '500',
    textAlign: 'center',
    fontSize: 15
  }
})

export default NoConnectivity;