import React, { Component } from 'react';
import { View, Text, Image, Alert, TextInput, ScrollView, TouchableOpacity, ImageBackground, PixelRatio ,ActivityIndicator} from 'react-native';
import { styles } from './styles';
import { bindActionCreators } from 'redux';
import { displayUserName } from '../../actions';
import { connect } from 'react-redux';
import {Loader} from '../../components';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });
import AesCrypto from 'react-native-aes-kit';
import AsyncStorage from '@react-native-community/async-storage';
import Tts from 'react-native-tts';
import { StackActions, NavigationActions } from 'react-navigation';
import {api_url} from '../../common'

const email_error = 'Please enter a valid email address.';
const password_error = 'Please enter a vali password.';

export class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //email: 'Survey',
      email: '',
      //password: 'Survey@2020',
      password: '',
      emailError: false,
      passwordError: false,
      showHide_passord: true,
      loading:false
    },
    global.savedRes = 0
  }
  storeData = async (token) => {
    try {
      console.log('success async==>'+JSON.stringify(token))
      await AsyncStorage.setItem('@storage_Key', token)
      await AsyncStorage.setItem('@email', this.state.email)
      //this.props.navigation.navigate('SurveyList')
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'SurveyList',params: { surveyoremail: this.state.email } })],
      });
      this.props.navigation.dispatch(resetAction);
    } catch (e) {
      console.log('Error in async==>'+JSON.stringify(e))
    }
  }
  newAes = (email,password) => {
    const plaintxt = `{"email":"${email}","password":"${password}"}`;//**'{"email":"chirag.shah@techforceglobal.com","password":"Survey@2020"}' */
    const secretKey = '4e3001083a3537db4339812ad033a01f';
    const iv = '4e3001083a3537db';
    let url = api_url + 'login?'
    AesCrypto.encrypt(plaintxt, secretKey, iv).then(cipher => {
      console.log('encrypted text:' + cipher);// return a string type cipher
      this.setState({ cipher,loading:true }, () => { console.log("ciper text: " + this.state.cipher) }
      );

      let details = {
        'authenticate': this.state.cipher,
      };
      let formBody = [];
      for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");

      var options = {
        method: 'POST',
        body: formBody,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        }
      }

      fetch(url,options).
      then((response)=>
        response.json().
        then((body)=>{
          console.log('Login Data'+JSON.stringify(body))
          this.setState({
            loading:false
          },()=>{
            if(body.success === true){
              this.storeData(body.token)
            }else{
              Alert.alert(
                '',
                body.message,
                [
                  {text: 'OK', onPress: () => console.log(body.message)},
                ],
                { cancelable: false }
              )
            }
            //this.props.navigation.navigate('SurveyList')
          })
        }).catch((error)=>
        this.setState({
          loading:false
        },()=>{
          console.log("API ERROR_RESPONSE: ", JSON.stringify(error))
        })
        )
      ).catch((error) => {
        this.setState({
          loading:false
        },()=>{
          console.log("API ERROR_OUTER: ", JSON.stringify(error));
          this.setState({loading:false},()=>{
            Alert.alert(
              '',
              'Oops, Something went wrong',
              [
                {text: 'OK', onPress: () => console.log('something went wrong')},
              ],
              { cancelable: false }
            )
          })
        })
      })
      
    }).catch(err => {
      console.log('encryption error:' + err);
    });
  }

  speak = () => {
    Tts.stop();
    Tts.speak(' كم يبلغ مرتبك الشهري؟\r\n');
  }

  componentDidMount() {
    global.mynavigation = this.props.navigation;
  }

  showHide_passord = () => {
    this.setState({ showHide_passord: !this.state.showHide_passord })
  }

  login = () => {
    let {email,password} = this.state
    this.newAes(email,password)
  }

  render() {
    const { email, password, showHide_passord, emailError, passwordError } = this.state;
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../../assets/images/logo.png')}
              style={styles.logoStyle}
            />
          </View>
          <View style={styles.LoginContainer}>
            <View style={styles.textBoxView}>
              <Image source={require('../../assets/images/email.png')} style={styles.textboxIconStyle} />
              <TextInput
                placeholder="Email"
                value={email}
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(email) => this.setState({ email })}
              />
            </View>
            {emailError && <Text style={styles.errorStyle}>{email_error}</Text>}
            <View style={styles.textBoxView}>
              <Image source={require('../../assets/images/lock.png')} style={styles.textboxIconStyle} />
              <TextInput
                placeholder="Password"
                value={password}
                placeholderTextColor="gray"
                style={styles.passwordTextInput}
                onChangeText={(password) => this.setState({ password })}
                secureTextEntry={showHide_passord ? true : false}
              />
              <TouchableOpacity onPress={this.showHide_passord}>
                {
                  showHide_passord ?
                    <Image source={require('../../assets/images/closeeye.png')} style={styles.textboxIconStyle} />
                    :
                    <Image source={require('../../assets/images/openeye.png')} style={styles.textboxIconStyle} />
                }
              </TouchableOpacity>
            </View>
            {passwordError && <Text style={styles.errorStyle}>{password_error}</Text>}
          </View>
          <TouchableOpacity style={styles.loginButtonStyle} onPress={this.login}>
            <Text style={styles.loginButtonText}>LOGIN</Text>
          </TouchableOpacity>
          <Text style={styles.surveyLinkStyle}>www.idg-survey.com</Text>
        </ScrollView>
        
        {
          this.state.loading &&
          <Loader />
        }
      </ImageBackground>
    )
  }
}

function mapStateToProps(state) {
  state = state.ReducerStore
  return {
    type: state.type,
    username: state.username
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ displayUserName }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);