import { View, Text, NativeModules, Dimensions, StyleSheet, PixelRatio, TouchableOpacity, ImageBackground } from 'react-native';
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

export const styles = StyleSheet.create({
    scrollViewStyle: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoContainer: {
        //flex:1,
        height: 120,
        width: 120,
        resizeMode: 'contain',
    },
    logoStyle: {
        flex: 1,
        height: undefined,
        width: undefined
    },
    LoginContainer: {
        //flex:1,
        width:'80%',
    },
    textBoxView: {
        marginTop:20,
        borderRadius:8,
        borderWidth:1,
        height:46,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal:10
    },
    textboxIconStyle:{
        height:20,width:20,resizeMode:'contain'
    },
    textInput:{
        flex:1,
        paddingHorizontal:10,
        fontFamily:'Poppins-Regular'
    },
    passwordTextInput:{
        paddingHorizontal:10,
        flex:1,
        fontFamily:'Poppins-Regular'
    },
    errorStyle:{
        color:'red',
        fontSize:12,
        marginTop:5,
        fontFamily:'Poppins-Regular'
    },
    loginButtonStyle:{
        height:40,
        width:'70%',
        borderRadius:65,
        backgroundColor:'#155f90',
        justifyContent:'center',
        alignItems:'center',
        marginTop:20,
    },
    loginButtonText:{
        color:'#fff',
        fontSize:15,
        fontFamily:'Poppins-Bold'
    },
    surveyLinkStyle:{
        color:'#155f90',
        fontSize:20,
        position:'absolute',
        alignSelf:'center',
        bottom:20,
        fontFamily:'Poppins-Regular',
        textAlign:'center',
        marginHorizontal:10
    },
    MainIndicatoreView:{
        flex:1, 
        position: 'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0 
    },
    BlackOpocityView:{ 
        backgroundColor:'#000',
        opacity: 0.2, 
        position: 'absolute', 
        top:0,
        bottom:0,
        left:0,
        right:0 
    },
    innerContainerView:{ 
        height: 90, 
        width: '45%',
        justifyContent:'center',
        backgroundColor:'#fff',
        marginTop:height/2 - 50,
        marginLeft:width/2-100,
        alignItems:'center'
    }
})