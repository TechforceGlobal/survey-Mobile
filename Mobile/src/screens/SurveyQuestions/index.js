import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ActivityIndicator, ScrollView, TouchableOpacity, ImageBackground, Alert } from 'react-native';
import { styles } from './styles';
import { Header, RadioGroup, MyStatusbar, Loader } from '../../components';
import Swiper from 'react-native-swiper';
import { NavigationActions, StackActions } from 'react-navigation';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';
import * as Animatable from 'react-native-animatable';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height;
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });
import RNFS from 'react-native-fs';
import {api_url} from '../../common'

export class SurveyQuestions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: -1,
      currIndex: 0,
      opacity: 0,
      surveyQuestions: [],
      surveyQuestions_common: [],
      mainSurveyArray: [],
      selectedLanguageCode: '',
      prevButtonText: '',
      commonData: [],
      commonData_en: [],
      surveyoremail: '',
      companyItem: [],
      locationItem: [],
      answers: [],
      assets:[]
    }
    this.swiper = null;
  }
  handleViewRef = ref => this.view = ref;
  async componentDidMount() {
    await this.getData()
    const { locationItem, surveyoremail, companyItem, surveyItem, selectedLanguageCode, Question, Question_common, commonData, commonData_en, assets } = this.props.navigation.state.params;
    let surveyQuestions = Question;
    let prevButtonText = commonData.previous !== '' ? commonData.previous : commonData_en.previous
    for (let i = 0; i < surveyQuestions.length; i++) {
      surveyQuestions[i].selectedAnswer = '';
      surveyQuestions[i].imageLoaded = true
    }
    this.setState({ companyItem, surveyoremail, locationItem, surveyQuestions, surveyQuestions_common: Question_common, selectedLanguageCode, prevButtonText, surveyItem, commonData, commonData_en, assets })
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key')
      if (value !== null) {
        this.setState({
          token: value
        })
      }
    } catch (e) {
      console.log('erro from Surveylist asynce' + JSON.stringify(e))
    }
  }

  setAnswer = (index, selectedOption) => {
    let surveyQuestions = this.state.surveyQuestions;
    let answers = this.state.answers;
    surveyQuestions[index].selectedAnswer = selectedOption;
    this.setState({ surveyQuestions }, () => {
      //console.log('when selected Array==>' + JSON.stringify(this.state.surveyQuestions[index]))
    })
    setTimeout(() => {
      this.setState({ opacity: 0 })
      if (index < surveyQuestions.length - 1) {
        let oue_ans_obj = {
          "question_id": surveyQuestions[index].quetion_id,
          "ans": selectedOption
        }
        answers.push(oue_ans_obj)
        this.setState({ currIndex: this.state.currIndex + 1, answers }, () => {
          answers
          global.consoleVar === true && console.log('answers==>' + JSON.stringify(this.state.answers))
        })
      } else {
        this.setState({ loading: true })
        let oue_ans_obj = {
          "question_id": surveyQuestions[index].quetion_id,
          "ans": selectedOption
        }
        answers.push(oue_ans_obj);
        this.setState({ answers }, () => {
          NetInfo.fetch().then(state => {
            if (state.isConnected) {
              this.saveRes()
              //this.saveToDb()
            } else {
              //alert('no connection');
              this.saveToDb()
            }
          });
        })
      }
    }, 300)
  }

  onPrev = async () => {
    if (this.state.currIndex !== 0) {
      let answers = this.state.answers;
      answers.pop();
      this.setState({ answers, currIndex: this.state.currIndex - 1 })
    } else {
      this.props.navigation.goBack();
    }
  }

  saveToDb = () => {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = (date.getMonth() + 1);

    if (gg < 10)
      gg = "0" + gg;

    if (mm < 10)
      mm = "0" + mm;

    var cur_day = aaaa + "-" + mm + "-" + gg;

    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds();

    if (hours < 10)
      hours = "0" + hours;

    if (minutes < 10)
      minutes = "0" + minutes;

    if (seconds < 10)
      seconds = "0" + seconds;

    let datetime = cur_day + " " + hours + ":" + minutes + ":" + seconds;
    const { companyItem, surveyoremail, locationItem, surveyItem, selectedLanguageCode, answers, commonData, commonData_en, assets } = this.state
    let ans_full_obj = {
      "SurveyorEmail": surveyoremail,
      "SurveyId": '' + surveyItem.id,
      "CompanyId": '' + companyItem.id,
      "LocationId": '' + locationItem.id,
      "Language": selectedLanguageCode,
      "offline": 1,
      "answers": answers,
      "datetime": datetime
    }
    let CompletedSurvey = []
    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO CompletedSurvey (AnswerJSON) VALUES (?)',
        [JSON.stringify(ans_full_obj)],
        (tx, results) => {
          global.savedRes = global.savedRes + 1
          db.transaction(tx => {
            tx.executeSql(
              'SELECT * FROM CompletedSurvey',
              [],
              (tx, results) => {

                for (let k = 0; k < results.rows.length; k++) {
                  CompletedSurvey.push(results.rows.item(k));
                }
                this.setState({ loading: false })
                db.transaction(tx => {
                  tx.executeSql(
                    'UPDATE CompanyLocation SET completedcount=? WHERE CompanyId=? AND id=?',
                    [locationItem.completedcount/* +1 */,companyItem.id,locationItem.id],
                    (tx, results) => {
                      this.props.navigation.replace('Thankyou', {
                        commonData, commonData_en,
                        survey_name: this.props.navigation.state.params.survey_name,
                        subSurveyTitle: this.props.navigation.state.params.subSurveyTitle,
                        surveyoremail: this.state.surveyoremail,
                        assets
                      })
                    }
                  );
                });
                global.consoleVar === true && console.log('CompletedSurvey: ' + JSON.stringify(CompletedSurvey));
              }
            );
          });
        }
      );
    });
  }

  saveRes = () => {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = (date.getMonth() + 1);

    if (gg < 10)
      gg = "0" + gg;

    if (mm < 10)
      mm = "0" + mm;

    var cur_day = aaaa + "-" + mm + "-" + gg;

    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds();

    if (hours < 10)
      hours = "0" + hours;

    if (minutes < 10)
      minutes = "0" + minutes;

    if (seconds < 10)
      seconds = "0" + seconds;

    let datetime = cur_day + " " + hours + ":" + minutes + ":" + seconds;
    const { companyItem, surveyoremail, locationItem, surveyItem, selectedLanguageCode, answers, commonData, commonData_en, assets } = this.state
    let ans_full_obj = {
      "SurveyorEmail": surveyoremail,
      "SurveyId": '' + surveyItem.id,
      "CompanyId": '' + companyItem.id,
      "LocationId": '' + locationItem.id,
      "Language": selectedLanguageCode,
      "offline": 0,
      "answers": answers,
      "datetime": datetime
    }
    var bodyData = [];
    bodyData.push(ans_full_obj)
    global.consoleVar === true && console.log('bodyData: ' + JSON.stringify(bodyData))
    var options = {
      method: 'POST',
      body: JSON.stringify(bodyData),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'token': this.state.token
      }
    }

    fetch(api_url+'storesurvey', options).
      then((response) =>
        response.json().
          then((body) => {
            global.consoleVar === true && console.log('Save api res: ' + JSON.stringify(body))
            this.setState({ loading: false })
            if (body.success === true) {
              db.transaction(tx => {
                tx.executeSql(
                  'UPDATE CompanyLocation SET completedcount=? WHERE CompanyId=? AND id=?',
                  [locationItem.completedcount/* +1 */,companyItem.id,locationItem.id],
                  (tx, results) => {
                    this.props.navigation.replace('Thankyou', {
                      commonData, commonData_en,
                      survey_name: this.props.navigation.state.params.survey_name,
                      subSurveyTitle: this.props.navigation.state.params.subSurveyTitle,
                      surveyoremail: this.state.surveyoremail,
                      assets
                    })
                  }
                );
              });
              /* this.props.navigation.replace('Thankyou', {
                commonData, commonData_en,
                survey_name: this.props.navigation.state.params.survey_name,
                subSurveyTitle: this.props.navigation.state.params.subSurveyTitle,
                surveyoremail: this.state.surveyoremail,
                assets
              }) */
            } else {
              Alert.alert(
                '',
                body.message,
                [
                  { text: 'OK', onPress: () => console.log(body.message) },
                ],
                { cancelable: false }
              )
            }
          }).catch((error) =>
            this.setState({
              loading: false
            }, () => {
              console.log("API ERROR_RESPONSE: ", JSON.stringify(error))
            })
          )
      ).catch((error) => {
        this.setState({
          loading: false
        }, () => {
          console.log("API ERROR_OUTER: ", JSON.stringify(error));
          this.setState({ loading: false }, () => {
            Alert.alert(
              '',
              'Oops, Something went wrong',
              [
                { text: 'OK', onPress: () => console.log('something went wrong') },
              ],
              { cancelable: false }
            )
          })
        })
      })
  }

  bounce = () => {
    this.setState({ opacity: 1 })
    this.view.bounceInRight(1500).then(endState => console.log(endState.finished ? 'bounce finished' : 'bounce cancelled'));
  }

  bounceOutUp = () => this.view.bounceOut(1500).then(endState => console.log(endState.finished ? 'bounce finished' : 'bounce cancelled'));

  render() {
    const { currIndex, index, surveyQuestions, surveyQuestions_common, opacity, selectedLanguageCode, commonData, assets } = this.state;
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName={this.props.navigation.state.params.survey_name} />
        {
          surveyQuestions !== undefined && surveyQuestions.length !== 0 && commonData.length !== 0 && assets.length !== 0 &&
          surveyQuestions.map((item, index) => {
            let Options = item.Options !== null && item.Options !== "" ? item.Options.split(",") : surveyQuestions_common[index].Options.split(",")
            if (surveyQuestions !== undefined) {
              return (
                currIndex === index &&
                <View onLayout={() => this.bounce()} style={{ opacity: opacity, flex: 1 }}>
                  <ScrollView contentContainerStyle={styles.scrollViewStyle}>
                    <View style={styles.logoContainer}>
                      <Image
                        source={{ uri: 'file://' + assets[parseInt(commonData.header_src)].image }}
                        style={styles.logoStyle}
                      />
                    </View>
                    <View style={styles.surveyImageContainer}>
                      <Image
                        source={{ uri: 'file://' + assets[parseInt(item.image)].image }}
                        style={styles.surveyImageStyle}
                      />
                    </View>
                    <Animatable.View ref={this.handleViewRef} style={{ flex: 1 }}>
                      {item.imageLoaded &&
                        <View style={styles.questionContainer}>
                          <Text style={styles.question}>Q{index + 1}.
                            {item.Question !== '' ?
                              item.Question
                              :
                              surveyQuestions_common[index].Question
                            }
                          </Text>
                        </View>
                      }
                      {item.imageLoaded &&
                        <View style={styles.answerContainer}>
                          <RadioGroup
                            assets={assets}
                            questionData={item}
                            item={Options}
                            index={index}
                            onPress={(selectedOption) => this.setAnswer(index, selectedOption)}
                            type={item.type}
                            selectedAnswer={item.selectedAnswer}
                          />
                        </View>
                      }
                      {!item.imageLoaded &&
                        <ActivityIndicator color="#165a87" />
                      }
                    </Animatable.View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                      <View style={styles.footerLogoContainer}>
                        <Image
                          source={{ uri: 'file://' + assets[parseInt(commonData.footer_src)].image }}
                          style={styles.footerLogoStyle}
                        />
                      </View>
                      <View style={styles.podLogoContainer}>
                        <Image
                          source={{ uri: 'file://' + assets[parseInt(commonData.footer2_src)].image }}
                          style={styles.podLogoStyle}
                        />
                      </View>
                    </View>
                  </ScrollView>
                </View>
              )
            }
          })
        }
        {
          <TouchableOpacity style={styles.prevButton} onPress={this.onPrev}>
            <Image source={require('../../assets/Icons/left.png')} style={{ height: 15, width: 20 }} />
            <Text numberOfLines={1} style={styles.prevButtonText}>{this.state.prevButtonText}</Text>
          </TouchableOpacity>
        }
        {
          this.state.loading &&
          <Loader />
        }
      </ImageBackground>
    )
  }
}

export default SurveyQuestions;