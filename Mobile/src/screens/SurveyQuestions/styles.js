import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
const {width,height} = Dimensions.get('window');
export const styles = StyleSheet.create({
    scrollViewStyle: {
        flexGrow: 1,
        padding: 16,
        //alignItems:'center'
    },
    logoContainer: {
        //flex: 1,
        alignSelf: 'center'
    },
    logoStyle: {
        // height: 80,
        // width: 80,
        height: 75,
        width: 75,
        resizeMode: 'contain',
    },
    LoginContainer: {
        width: '80%',
    },
    surveyImageContainer: {
        alignSelf: 'center',
        justifyContent:'center',
        alignItems:'center'
    },
    surveyImageStyle: {
        marginVertical:10,
        //height: 100,
        height: 170,
        width: width-24,
        resizeMode: 'contain',
    },
    footerLogoContainer: {
        
    },
    footerLogoStyle:{
        height: 50,
        width: 50,
        resizeMode: 'contain',
    },
    podLogoContainer:{
        marginLeft:10,
        justifyContent:'center',
        alignItems:'center',
        //borderWidth:1
    },
    podLogoStyle:{
        height: 210,
        width: 210,
        resizeMode: 'contain',
    },
    textBoxView: {
        marginTop: 20,
        borderRadius: 8,
        borderWidth: 1,
        height: 46,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    textInput: {
        paddingHorizontal: 10
    },
    passwordTextInput: {
        paddingHorizontal: 10,
        flex: 1
    },
    errorStyle: {
        color: 'red',
        fontSize: 12,
        marginTop: 5
    },
    submitButton: {
        height: 40,
        width: '70%',
        borderRadius: 65,
        backgroundColor: '#155f90',
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'center'
    },
    submitButtonText: {
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold'
    },
    prevButton: {
        height: 40,
        width: '35%',
        borderRadius: 65,
        backgroundColor: '#155f90',
        //justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        margin: 10,
        flexDirection: 'row',
        elevation: 5
    },
    prevButtonText: {
        color: '#fff',
        fontSize: 15,
        //marginTop:5,
        fontFamily:'Poppins-Bold',
        marginHorizontal: 10,
        paddingRight:10
    },
    swiperContainer: {

    },
    questionContainer: {
        flexDirection: 'row'
    },
    question: {
        color: '#000',
        //fontSize: 20,
        fontSize: 22,
        fontFamily:'Poppins-Bold'
    },
    answerContainer: {
        flex: 1,
        //marginTop: 10,
    },
})