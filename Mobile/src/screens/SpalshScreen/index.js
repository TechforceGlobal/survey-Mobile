import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
export class SpalshScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      emailError: false,
      passwordError: false,
      showHide_passord: true,
      email:''
    }
    global.consoleVar = true
  }

  async componentDidMount() {
    global.mynavigation = this.props.navigation;
    try {
     let email = await AsyncStorage.getItem('@email')
     if(email!==null && email!==undefined){
       setTimeout(()=>{
         const resetAction = StackActions.reset({
           index: 0,
           actions: [NavigationActions.navigate({ routeName: 'SurveyList',params: { surveyoremail: email } })],
         });
         this.props.navigation.dispatch(resetAction);
       },1000)
     }else{
      setTimeout(()=>{
        mynavigation.navigate('Login')
      },1000)
     }
    } catch (e) {
      console.log('Error in async==>'+JSON.stringify(e))
    }
  }

  render() {
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{flex:1}}>
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../../assets/images/logo.png')}
              style={styles.logoStyle}
            />
          </View>
          <View>
            <Text style={styles.surveyLinkStyle}>www.idg-survey.com</Text>
          </View>
        </ScrollView>
      </ImageBackground>
    )
  }
}

export default SpalshScreen;