import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export const styles = StyleSheet.create({
    scrollViewStyle: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoContainer: {
        flex:1,
        justifyContent:'center'
        //backgroundColor:'red'
    },
    logoStyle: {
        height: 120,
        width: 120,
        resizeMode: 'contain',
    },
    surveyLinkStyle:{
        color:'#155f90',
        fontSize:20,
        marginBottom:20,
        fontFamily:'Poppins-Regular'
    }
})