import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export const styles = StyleSheet.create({
    scrollViewStyle: {
        flexGrow: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    logoContainer: {
        //flex:1,
        alignItems:'center'
    },
    logoStyle: {
        height: 120,
        width: 120,
        resizeMode: 'contain',
    },
    LoginContainer: {
        //flex:1,
        width:'80%',
    },
    bottomLogoContainer:{
        //flex:1,
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    /* footerLogoContainer:{
        
    }, */
    footerLogoStyle:{
        height: 50,
        width: 50,
        resizeMode: 'contain',
    },
    /* podLogoContainer:{
        height: 150,
        width: 150,
        resizeMode: 'contain',
        marginLeft:10
    }, */
    podLogoStyle:{
        // height: 150,
        // width: 150,
        height: 210,
        width: 210,
        resizeMode: 'contain',
        marginLeft:10
    },
    textBoxView: {
        marginTop:20,
        borderRadius:8,
        borderWidth:1,
        height:46,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal:10
    },
    textInput:{
        paddingHorizontal:10
    },
    buttonContainer:{
        width:'100%',
        alignItems:'center',
        //flex:1
    },
    loginButtonStyle:{
        height:40,
        width:'70%',
        borderRadius:65,
        backgroundColor:'#155f90',
        justifyContent:'center',
        alignItems:'center',
        marginTop:20,
        paddingHorizontal:10
    },
    loginButtonText:{
        color:'#fff',
        fontSize:15,
        fontFamily:'Poppins-Bold'
    },
    languageButtonStyle:{
        height:40,
        width:'70%',
        borderRadius:65,
        backgroundColor:'#155f90',
        justifyContent:'center',
        alignItems:'center',
        marginTop:10,
        paddingHorizontal:10
    },
    welcomeTextStyle:{
        fontSize:17,
        fontFamily:'Poppins-Regular',
        color:'#404041',
        marginTop:20,
        textAlign:'center',
        marginHorizontal:24,
    },
    surveyTitleStyle:{
        fontSize:20,
        fontFamily:'Poppins-Bold',
        color:'#28b4e1',
        marginVertical:10,
        borderBottomWidth:2,
        borderColor:'#28b4e1'
    },
})