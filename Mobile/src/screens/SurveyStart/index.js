import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import { Header, MyStatusbar, Loader } from '../../components';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });
import { WebView } from 'react-native-webview';
export class SurveyStart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      commonData: [],
      commonData_en: [],
      surveyItem: [],
      subSurveyItem: [],
      selectedLanguageCode: '',
      Question: [],
      Question_common: [],
      loading: true,
      surveyoremail: '',
      companyItem: [],
      locationItem: [],
      subSurveyTitle:'',
      assets:[]
    }
  }

  componentDidMount() {
    const { subSurveyTitle,surveyItem, selectedLanguageCode, surveyoremail, companyItem, locationItem, assets } = this.props.navigation.state.params;
    this.setState({ subSurveyTitle,surveyItem, selectedLanguageCode, surveyoremail, companyItem, locationItem, assets }, () => {
      this.loadCommonData()
    })
  }

  loadCommonData = () => {
    const { surveyItem, selectedLanguageCode, companyItem, locationItem } = this.state;
    let commonData = []
    let commonData_en = []
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM common WHERE surveyId = ? AND companyId = ? AND locationId = ? AND lang_code = ?',
        [parseInt(surveyItem.id), (companyItem.id).toString(), (locationItem.id).toString(), selectedLanguageCode],
        (tx, results) => {
          for (let j = 0; j < results.rows.length; j++) {
            commonData.push(results.rows.item(j));
          }
          global.consoleVar === true && console.log('common data: ' + JSON.stringify(commonData[0]))

          //English data
          db.transaction(tx => {
            tx.executeSql(
              'SELECT * FROM common WHERE surveyId = ? AND companyId = ? AND locationId = ? AND lang_code = ?',
              [parseInt(surveyItem.id), (companyItem.id).toString(), (locationItem.id).toString(), 'en'],
              (tx, results) => {
                for (let j = 0; j < results.rows.length; j++) {
                  commonData_en.push(results.rows.item(j));
                }
                global.consoleVar === true && console.log('common data en: ' + JSON.stringify(commonData_en[0]))
                this.setState({ commonData: commonData[0], commonData_en: commonData_en[0] })
              }
            );
          });

        }
      );
    });

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM Question WHERE surveyId = ? AND companyId = ? AND locationId = ? AND lang_code = ? ORDER BY orderId',
        [parseInt(surveyItem.id), (companyItem.id).toString(), (locationItem.id).toString(), selectedLanguageCode],
        (tx, results1) => {
          let Question = []
          for (let m = 0; m < results1.rows.length; m++) {
            Question.push(results1.rows.item(m));
          }
          global.consoleVar === true && console.log('Question: ' + JSON.stringify(Question))
          this.setState({ Question, loading: false })
        }
      );
    });

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM Question WHERE surveyId = ? AND companyId = ? AND locationId = ? AND lang_code = ? ORDER BY orderId',
        [parseInt(surveyItem.id), (companyItem.id).toString(), (locationItem.id).toString(), 'en'],
        (tx, results1) => {
          let Question_common = []
          for (let m = 0; m < results1.rows.length; m++) {
            Question_common.push(results1.rows.item(m));
          }
          global.consoleVar === true && console.log('Question_common: ' + JSON.stringify(Question_common))
          this.setState({ Question_common, loading: false })
        }
      );
    });

  }

  startSurvey = () => {
    const { survey_name } = this.props.navigation.state.params;
    const {assets,subSurveyTitle, surveyItem, selectedLanguageCode, Question, Question_common, commonData, commonData_en, locationItem, companyItem, surveyoremail } = this.state;
    var d = new Date()
    var month = d.getMonth() + 1
    var day = d.getDate()
    var year = d.getFullYear();

    var exp = new Date(commonData.expiryDate)
    var month_e = exp.getMonth() + 1;
    var day_e = exp.getDate();
    var year_e = exp.getFullYear();
    let expired = true
    if (!(year > year_e)) {
      //alert(month + ',' + day + ', ' + year + ', ' + month_e + ', ' + day_e + ', ' + year_e)
      if (!(month > month_e)) {
        if (month === month_e) {
          if (!(day > day_e)) {
            expired = false
          }
        } else {
          expired = false
        }
      }
    }
    if (expired) {
      alert('This survey is expired')
    } else {
      this.props.navigation.navigate('SurveyQuestions', {
        survey_name,
        subSurveyTitle,
        selectedLanguageCode,
        surveyItem,
        Question,
        Question_common,
        commonData,
        commonData_en,
        locationItem,
        companyItem,
        surveyoremail,
        assets
      })
    }
  }

  chaneLanguage = () => {
    this.props.navigation.goBack();
  }

  render() {
    const { questionArray, selectedLanguageCode } = this.props.navigation.state.params;
    const { commonData, commonData_en, subSurveyTitle, assets} = this.state;
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName={this.props.navigation.state.params.survey_name} />
        {
          assets.length !== 0 && commonData.length !== 0 && commonData_en.length !== 0 &&
          <ScrollView contentContainerStyle={styles.scrollViewStyle}>
            <View style={styles.logoContainer}>
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.header_src)].image }}
                style={styles.logoStyle}
              />
              {/* <Text style={styles.surveyTitleStyle}>{this.props.navigation.state.params.subSurveyTitle}</Text> */}
              <View style={{ height: 50, width: 300, marginTop: 10 }}>
                <WebView source={{ html: `<h2 style="font-size:70px;text-align:center;color:#28b4e1">${subSurveyTitle}</h2>` }}
                  style={{ backgroundColor: 'transparent' }}
                />
              </View>
            </View>
            {commonData.length !== 0 && commonData_en.length !== 0 &&
              <View style={styles.buttonContainer}>
                <Text style={styles.welcomeTextStyle}>
                  {commonData.Welcometext !== '' ?
                    commonData.Welcometext
                    :
                    commonData_en.Welcometext
                  }
                </Text>
                <TouchableOpacity style={styles.loginButtonStyle} onPress={this.startSurvey}>
                  <Text style={styles.loginButtonText} numberOfLines={1}>
                    {commonData.startsurvey !== '' ?
                      commonData.startsurvey
                      :
                      commonData_en.startsurvey
                    }
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.languageButtonStyle} onPress={this.chaneLanguage}>
                  <Text style={styles.loginButtonText} numberOfLines={1}>
                    {commonData.changelanguage !== '' ?
                      commonData.changelanguage
                      :
                      commonData_en.changelanguage
                    }
                  </Text>
                </TouchableOpacity>
              </View>
            }

            <View style={styles.bottomLogoContainer}>
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.footer_src)].image }}
                style={styles.footerLogoStyle}
              />
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.footer2_src)].image }}
                style={styles.podLogoStyle}
              />
            </View>
            {
              this.state.loading &&
              <Loader />
            }
          </ScrollView>
        }
      </ImageBackground>
    )
  }
}

export default SurveyStart;