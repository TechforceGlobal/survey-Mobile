import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import { MyStatusbar, Header } from '../../components';
import { StackActions, NavigationActions } from 'react-navigation';
import { WebView } from 'react-native-webview';
export class Thankyou extends Component {
  constructor(props) {
    super(props)
    this.state = {
      commonData: [],
      commonData_en: [],
      survey_name: '',
      surveyoremail: [],
      assets: []
    }
  }

  componentDidMount() {
    const { commonData, commonData_en, survey_name, surveyoremail, assets } = this.props.navigation.state.params;
    this.setState({ commonData, commonData_en, survey_name, surveyoremail, assets })
  }

  goToSurvey = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'SurveyList', params: { surveyoremail: this.state.surveyoremail } })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    const { commonData, commonData_en, survey_name, assets } = this.state;
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName={survey_name} />
        {
          assets.length !== 0 && commonData.length !== 0 && commonData_en.length !== 0 &&
          <ScrollView contentContainerStyle={styles.scrollViewStyle}>
            <View style={styles.logoContainer}>
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.header_src)].image }}
                style={styles.logoStyle}
              />
              {/* <Text style={styles.surveyTitleStyle}>{this.props.navigation.state.params.subSurveyTitle}</Text> */}
              <View style={{ height: 50, width: 300, marginTop: 10 }}>
                <WebView source={{ html: `<h2 style="font-size:70px;text-align:center;color:#28b4e1">${this.props.navigation.state.params.subSurveyTitle}</h2>` }}
                  style={{ backgroundColor: 'transparent' }}
                />
              </View>
            </View>
            <View style={styles.buttonContainer}>
              <Text style={styles.welcomeTextStyle}>
                {commonData.thankyoutext !== '' ?
                  commonData.thankyoutext
                  :
                  commonData_en.thankyoutext
                }
              </Text>
              <TouchableOpacity style={styles.loginButtonStyle} onPress={this.goToSurvey}>
                <Text style={styles.loginButtonText}>Go To Survey</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.bottomLogoContainer}>
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.footer_src)].image }}
                style={styles.footerLogoStyle}
              />
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.footer2_src)].image }}
                style={styles.podLogoStyle}
              />
            </View>
          </ScrollView>
        }
      </ImageBackground>
    )
  }
}

export default Thankyou;