import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export const styles = StyleSheet.create({
    scrollViewStyle: {
        flexGrow: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    logoContainer: {
        alignItems:'center'
    },
    logoStyle: {
        height: 120,
        width: 120,
        resizeMode: 'contain',
    },
    LoginContainer: {
        width:'80%',
    },
    mainContent:{
        //marginTop:10,
        alignItems:'center'
    },
    bottomLogoContainer:{
        flex:1,
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    /* footerLogoContainer:{
        
    }, */
    footerLogoStyle:{
        height: 50,
        width: 50,
        resizeMode: 'contain',
    },
    /* podLogoContainer:{
        height: 150,
        width: 150,
        resizeMode: 'contain',
        marginLeft:10
    }, */
    podLogoStyle:{
        // height: 150,
        // width: 150,
        height: 210,
        width: 210,
        resizeMode: 'contain',
        marginLeft:10
    },
    textBoxView: {
        marginTop:20,
        borderRadius:8,
        borderWidth:1,
        height:46,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal:10
    },
    textInput:{
        paddingHorizontal:10
    },
    passwordTextInput:{
        paddingHorizontal:10,
        flex:1
    },
    errorStyle:{
        color:'red',
        fontSize:12,
        marginTop:5
    },
    loginButtonStyle:{
        height:40,
        width:'70%',
        borderRadius:65,
        backgroundColor:'#155f90',
        justifyContent:'center',
        alignItems:'center',
        marginTop:20
    },
    loginButtonText:{
        color:'#fff',
        fontSize:15,
        fontWeight:'bold'
    },
    surveyTitleStyle:{
        fontSize:20,
        fontFamily:'Poppins-Bold',
        color:'#28b4e1',
        marginVertical:10,
        borderBottomWidth:2,
        borderColor:'#28b4e1'
    },
    selectLanguageLabelStyle:{
        fontSize:20,
        fontFamily:'Poppins-Regular',
        color:'#404041',
        marginVertical:10,
    },
    selectLanguageBoxStyle:{
        flexDirection:'row',
        flexWrap:'wrap',
        justifyContent:'center'
    },
    languageItem:{
        backgroundColor:'#155f90',
        width:'30%',
        height:50,
        margin:'2%',
        justifyContent:'center',
        paddingHorizontal:10
    },
    languageItemText:{
        color:'#fff',
        textAlign:'center',
        fontFamily:'Poppins-Regular'
    },
    noDataTextStyle:{
        color:'#000',
        fontSize:18,
        textAlign:'center',
        fontFamily:'Poppins-Bold'
    }
})