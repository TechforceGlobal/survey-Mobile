import React, { Component } from 'react';
import { View, Text, Image, Dimensions, Alert, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import { Header, Loader, MyStatusbar } from '../../components';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });
import { WebView } from 'react-native-webview';

export class LanguageSelection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      token: '',
      language_array: [],
      wholeSurveyArray: [],
      loading: false,
      selectedLanguage: '',
      selectedLanguageCode: '',
      surveyItem: [],
      subSurveyItem: [],
      languages_array: [],
      commonData: [],
      surveyoremail: '',
      companyItem: [],
      locationItem: [],
      subSurveyTitle: '',
      assets:[]
    }
  }

  async componentDidMount() {
    const { surveyItem, surveyoremail, companyItem, locationItem, survey_name } = this.props.navigation.state.params;
    this.setState({ surveyItem, surveyoremail, companyItem, locationItem, loading: true }, () => {
      let assets = []
      db.transaction(tx => {
        tx.executeSql(
          'SELECT * FROM assets',
          [],
          (tx, results1) => {
            for (let m = 0; m < results1.rows.length; m++) {
              assets.push(results1.rows.item(m));
            }
            global.consoleVar === true && console.log('assets: ' + JSON.stringify(assets))
            this.setState({ assets})
          }
        );
      });
      
      db.transaction(tx => {
        tx.executeSql(
          'SELECT subsurveyTitle FROM SubSurvey WHERE survey_id=? AND companyId=? AND locationId=?',
          [parseInt(surveyItem.id), parseInt(companyItem.id), parseInt(locationItem.id)],
          (tx, results) => {
            this.setState({ subSurveyTitle: results.rows.item(0).subsurveyTitle }, () => {

            })
            global.consoleVar === true && console.log('SubSurvey: ' + JSON.stringify(results.rows.item(0)))
            this.LoadLanguages()
          }
        );
      });
    })
  }

  LoadLanguages = () => {
    const { surveyItem, companyItem, locationItem } = this.state;
    let langArray = []
    let languages_array = []
    let commonData = []
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM common WHERE surveyId = ? AND companyId = ? AND locationId = ?',
        [parseInt(surveyItem.id), (companyItem.id).toString(), (locationItem.id).toString()],
        (tx, results) => {
          for (let j = 0; j < results.rows.length; j++) {
            commonData.push(results.rows.item(j));
          }
          global.consoleVar === true && console.log('common data: ' + JSON.stringify(commonData))
          if (commonData.length !== 0) {
            commonData = commonData[0]
          }
          //Language data
          db.transaction(tx => {
            tx.executeSql(
              'SELECT * FROM Language WHERE surveyId = ? AND companyId = ? AND locationId = ?',
              [parseInt(surveyItem.id), (companyItem.id).toString(), (locationItem.id).toString()],
              (tx, results) => {

                for (let j = 0; j < results.rows.length; j++) {
                  langArray.push(results.rows.item(j));
                }
                global.consoleVar === true && console.log('langArray: ' + JSON.stringify(langArray))
                let language_codes = []
                let language_values = []
                if (langArray.length !== 0) {
                  language_codes = langArray[0].language_codes.split(",")
                  language_values = langArray[0].language_values.split(",")
                  for (let i = 0; i < language_values.length; i++) {
                    let obj = { "lang_code": language_codes[i], "lang_value": language_values[i] }
                    languages_array.push(obj)
                  }
                }

                global.consoleVar === true && console.log('languages_array: ' + JSON.stringify(languages_array))
                this.setState({ languages_array, commonData: commonData, loading: false })
              }
            );
          });

        }
      );
    });

  }

  onSelectLanguage = (item) => {
    const { surveyItem, surveyoremail, companyItem, locationItem, subSurveyTitle, assets} = this.state
    this.props.navigation.navigate('SurveyStart', {
      survey_name: this.props.navigation.state.params.survey_name,
      subSurveyTitle: subSurveyTitle,
      selectedLanguage: item.lang_value,
      selectedLanguageCode: item.lang_code,
      questionArray: this.state.wholeSurveyArray,
      surveyItem,
      surveyoremail,
      companyItem,
      locationItem,
      assets
    })
  }

  render() {
    const { commonData, languages_array, subSurveyTitle, assets } = this.state;
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName={this.props.navigation.state.params.survey_name} />
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
          <View style={styles.logoContainer}>
            {commonData.length !== 0 && assets.length !== 0 && <Image
              source={{ uri: 'file://' + assets[parseInt(commonData.header_src)].image }}
              style={styles.logoStyle}
            />}
            {/* <Text style={styles.surveyTitleStyle}>{this.props.navigation.state.params.survey_name}</Text> */}
            <View style={{ height: 50, width: 300, marginTop: 10 }}>
              <WebView source={{ html: `<h2 style="font-size:70px;text-align:center;color:#28b4e1">${subSurveyTitle}</h2>` }}
                style={{ backgroundColor: 'transparent' }}
              />
            </View>
          </View>
          <View style={styles.mainContent}>
            <Text style={styles.selectLanguageLabelStyle}>Select Language</Text>
            <View style={styles.selectLanguageBoxStyle}>
              {
                languages_array.length !== 0 && languages_array.map((item, index) => {
                  return (
                    <TouchableOpacity style={styles.languageItem} onPress={() => this.onSelectLanguage(item)}>
                      <Text style={styles.languageItemText}>{item.lang_value}</Text>
                    </TouchableOpacity>
                  )
                })
              }
            </View>
          </View>

          {commonData.length !== 0 && assets.length !== 0 && 
            <View style={styles.bottomLogoContainer}>
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.footer_src)].image }}
                style={styles.footerLogoStyle}
              />
              <Image
                source={{ uri: 'file://' + assets[parseInt(commonData.footer2_src)].image }}
                style={styles.podLogoStyle}
              />
            </View>}
        </ScrollView>
        {
          this.state.loading &&
          <Loader />
        }
      </ImageBackground>
    )
  }
}

export default LanguageSelection;