import React, { Component } from 'react';
import { View, Text, Image, Alert, StatusBar, ScrollView, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';
import { styles } from './styles';
import { Header, SearchBox, Breadcumb, Loader, MyStatusbar, Popup } from '../../components';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });

export class CompanyList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchValue: '',
      survey_name: '',
      routes: ['Home'],
      searchResult: [],
      company_list_array: [],
      token: '',
      loading: false,
      surveyItem:[],
      subSurveyItem:[],
      surveyoremail:''
    }
  }

  async componentDidMount() {
    const { surveyItem, surveyoremail } = this.props.navigation.state.params;
    this.setState({ survey_name:surveyItem.survey, CompanyIds:surveyItem.CompanyIds, surveyItem }, () => {
      var routes = this.state.routes;
      routes.push(this.state.survey_name)
      this.setState({ routes, loading: true, surveyoremail }, () => {
        this.fetchCompanyList()
      })
    })
  }

  fetchCompanyList = () => {
    let company_list_array = []
    const {CompanyIds} = this.state;
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM Company WHERE id in (${CompanyIds}) group by id`,
        [],
        (tx, results) => {
          for (let k = 0; k < results.rows.length; k++) {
            company_list_array.push(results.rows.item(k));
          }
          global.consoleVar === true && console.log('where in companies: ' + JSON.stringify(company_list_array))
          this.setState({company_list_array,loading:false})
        }
      );
    });
  }

  search = (searchValue) => {
    let searchResult = [];
    this.setState({ searchValue: searchValue })
    for (let i = 0; i < this.state.company_list_array.length; i++) {
      if ((this.state.company_list_array[i].name.toLowerCase()).includes(searchValue.toLowerCase())) {
        searchResult.push(this.state.company_list_array[i]);
      }
    }
    this.setState({ searchResult })
  }

  navigateToCopanyList = (item) => {
    const { surveyItem, surveyoremail, survey_name } = this.state
    this.props.navigation.navigate('LocationList', {
      company_name: item.name,
      survey_name: survey_name,
      surveyItem,
      location_array: item.locations,
      companyItem:item,
      surveyoremail
    })
  }

  render() {
    const { searchValue, routes, searchResult, company_list_array } = this.state;
    let array = searchValue === '' ? company_list_array : searchResult
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName={this.state.survey_name} />
        <Breadcumb routes={routes} />
        <SearchBox
          for="Company"
          searchValue={searchValue}
          search={this.search}
        />
        <View style={styles.titleContainer}>
          <Text style={styles.headerText}>Select Company</Text>
        </View>
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
          <View style={styles.listStyle}>
            {
              array.length !== 0 && array.map((item, index) => {
                return (
                  <TouchableOpacity style={styles.listItemStyle} onPress={() => this.navigateToCopanyList(item)}>
                    <Text style={styles.listItemTextStyle}>{item.name}</Text>
                    <Image source={require('../../assets/images/blueaero.png')} style={styles.textboxIconStyle} />
                  </TouchableOpacity>
                )
              })
            }
          </View>
        </ScrollView>
        {
          this.state.loading &&
          <Loader />
        }
      </ImageBackground>

    )
  }
}

export default CompanyList;