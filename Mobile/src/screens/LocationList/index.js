import React, { Component } from 'react';
import { View, Text, Image, Alert, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import { Header, SearchBox, Breadcumb, Loader, MyStatusbar } from '../../components';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });

export class LocationList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchValue: '',
      routes: [],
      company_name: '',
      routes: ['Home'],
      searchResult: [],
      locations_array: [],
      loading: true,
      surveyItem: [],
      companyItem: [],
      subSurveyItem: [],
      surveyoremail: '',
      survey_name: ''
    }
  }

  componentDidMount() {
    const { survey_name, company_name, companyItem, surveyItem, surveyoremail } = this.props.navigation.state.params;
    let routes = this.state.routes;
    let locations = this.props.navigation.state.params.location_array

    let compIds = surveyItem.CompanyIds
    var compArray = compIds.split(",")
    global.consoleVar === true && console.log('compArray: ' + JSON.stringify(compArray))

    let locaIds = surveyItem.locationIds
    var locaArray = locaIds.split(",")
    global.consoleVar === true && console.log('locaArray: ' + JSON.stringify(locaArray))
    let locaString = ''
    for (let c = 0; c < compArray.length; c++) {
      if (compArray[c] === "" + companyItem.id) {
        if (locaString === '') {
          locaString = locaString + locaArray[c]
        } else {
          locaString = locaString + ',' + locaArray[c]
        }
      }
    }
    global.consoleVar === true && console.log('locaString: ' + JSON.stringify(locaString))


    this.setState({ survey_name, company_name, surveyItem, companyItem, surveyoremail }, () => {
      this.fetchLocations(locaString)
      routes.push(survey_name, company_name)
      this.setState({ routes })
    })
  }

  fetchLocations = (locaString) => {
    const { companyItem, surveyItem } = this.state;
    let locations_array = []
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM CompanyLocation WHERE CompanyId = ? and id in (${locaString}) group by id`,
        [companyItem.id],
        (tx, results) => {
          for (let k = 0; k < results.rows.length; k++) {
            locations_array.push(results.rows.item(k));
          }
          this.setState({ locations_array, loading: false })
          console.log('locations_array: ' + JSON.stringify(locations_array))
        }
      );
    });
  }

  search = (searchValue) => {
    let searchResult = [];
    this.setState({ searchValue: searchValue })
    for (let i = 0; i < this.state.locations_array.length; i++) {
      if ((this.state.locations_array[i].name.toLowerCase()).includes(searchValue.toLowerCase())) {
        searchResult.push(this.state.locations_array[i]);
      }
    }
    this.setState({ searchResult })
  }

  navigateToCopanyList = (item) => {
    if(item.headcount>item.completedcount){
      const { surveyItem, surveyoremail, companyItem, survey_name } = this.state;
      this.props.navigation.navigate('LanguageSelection', {
        survey_name,
        surveyItem,
        surveyoremail,
        companyItem,
        locationItem: item,
      })
    }else{
      Alert.alert(
        '',
        'You have completed maximum number of surveys at this location.',
        [
          { text: 'OK', onPress: () => console.log('something went wrong') },
        ],
        { cancelable: false }
      )
    }
  }

  render() {
    const { searchValue, company_name, routes, searchResult, locations_array } = this.state;
    let array = searchValue === '' ? locations_array : searchResult
    return (
      <ImageBackground source={require('../../assets/images/backgroundImage.jpg')} style={{ flex: 1 }}>
        <MyStatusbar />
        <Header headerName={company_name} />
        <Breadcumb routes={routes} />
        <SearchBox
          for="Location"
          searchValue={searchValue}
          search={this.search}
        />
        <View style={styles.titleContainer}>
          <Text style={styles.headerText}>Select Location</Text>
        </View>
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
          <View style={styles.listStyle}>
            {
              array.length !== 0 && array.map((item, index) => {
                return (
                  <TouchableOpacity style={styles.listItemStyle} onPress={() => this.navigateToCopanyList(item)}>
                    <Text style={styles.listItemTextStyle}>{item.name}</Text>
                    <Image source={require('../../assets/images/blueaero.png')} style={styles.textboxIconStyle} />
                  </TouchableOpacity>
                )
              })
            }
          </View>
        </ScrollView>
      </ImageBackground>
    )
  }
}

export default LocationList;