import React, { Component } from 'react';
import { View, Text, Image, StatusBar, TextInput, ScrollView, TouchableOpacity, ImageBackground,Platform } from 'react-native';
export class MyStatusbar extends Component {
  
  render() {
    return (
        <View
          style={{
            backgroundColor: '#165a87',
            height: Platform.OS === 'ios' ? 30 : StatusBar.currentHeight,
          }}>
          <StatusBar
            translucent
            backgroundColor="#165a87"
            barStyle="light-content"
          />
        </View>
       
    )
  }
}

export default MyStatusbar;