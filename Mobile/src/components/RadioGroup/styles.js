import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export const styles = StyleSheet.create({
    /* optionStyle:{
        color:'#404041',
        fontSize:18,
        fontWeight:'bold'
    }, */
    radioContainer:{
        flexDirection: 'row', 
        marginTop: 12,
        backgroundColor:'#c5c5c5',
        padding:8
    },
    selectedOuterRadio: {
        height: 24, width: 24, borderRadius: 12, borderWidth: 2, justifyContent:'center',alignItems:'center'
    },
    outerRadio: {
        height: 24, width: 24, borderRadius: 12, borderWidth: 1,justifyContent:'center',alignItems:'center'
    },
    selectedInnerRadio: {
        height: 14, width: 14, borderRadius: 14, backgroundColor: '#404041'
    },
    innerRadio: {
        
    },
    optionStyle:{
        marginLeft: 10, 
        fontSize: 20,
        fontFamily:'Poppins-Regular'
    },
})