import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
const agree_disagree_array = [
    {option:'strongly agree',image:2,code:4},{option:'agree',image:3,code:3},{option:'disagree',image:5,code:2},{option:'strongly disagree',image:4,code:1}
]
const yes_no_array = [
    {option:'yes',image:0,code:4},{option:'no',image:1,code:1}
]
export class RadioGroup extends Component {

    render() {
        const { item, index, onPress, imageAgree, imageDis, type, selectedAnswer, questionData, /* alldata */assets } = this.props;
        return (
            <View>
                {
                    type === "custom" &&
                    item!==null ?
                    item.length!==0 && item.map((option, optionIndex) => {
                        return (
                            <TouchableOpacity style={styles.radioContainer}
                                onPress={() =>
                                    onPress(option)
                                }
                            >
                                <View style={questionData.selectedAnswer === option ? styles.selectedOuterRadio : styles.outerRadio}>
                                    <View style={questionData.selectedAnswer === option ? styles.selectedInnerRadio : styles.innerRadio}>

                                    </View>
                                </View>
                                <Text style={styles.optionStyle}>{option}</Text>
                            </TouchableOpacity>
                        )
                    })
                    :
                    null
                }
                {
                    type === "agree/disagree" &&
                    agree_disagree_array.map((option, optionIndex) => {
                        return (
                            <TouchableOpacity style={[styles.radioContainer,{alignItems:'center'}]}
                                onPress={() =>
                                    onPress(option.code)
                                }
                            >
                                <View style={questionData.selectedAnswer === option.code ? styles.selectedOuterRadio : styles.outerRadio}>
                                    <View style={questionData.selectedAnswer === option.code ? styles.selectedInnerRadio : styles.innerRadio}>

                                    </View>
                                </View>
                                <Image 
                                    source={{
                                        uri:'file://'+assets[option.image].image
                                    }}
                                    style={{height:50,width:50,resizeMode:'contain',marginLeft:10}}
                                />
                            </TouchableOpacity>
                        )
                    })
                }
                {
                    type === "yesno" &&
                    yes_no_array.map((option, optionIndex) => {
                        return (
                            <TouchableOpacity style={[styles.radioContainer,{alignItems:'center'}]}
                                onPress={() =>
                                    onPress(option.code)
                                }
                            >
                                <View style={questionData.selectedAnswer === option.code ? styles.selectedOuterRadio : styles.outerRadio}>
                                    <View style={questionData.selectedAnswer === option.code ? styles.selectedInnerRadio : styles.innerRadio}>

                                    </View>
                                </View>
                                <Image 
                                    source={{
                                        uri:'file://'+assets[option.image].image
                                    }}
                                    style={{height:50,width:50,resizeMode:'contain',marginLeft:10}}
                                />
                            </TouchableOpacity>
                        )
                    })
                }
            </View>

        )
    }
}

export default RadioGroup;