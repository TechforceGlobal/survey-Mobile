import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export const styles = StyleSheet.create({
    searchBox:{
        borderRadius:35,
        borderWidth:1,
        height:44,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal:10,
        marginTop:15,
        marginHorizontal:12,
    },
    textInput:{
        //backgroundColor:'red',
        flex:1,
        height:44,
        paddingHorizontal:10,
        marginTop:8,
        fontSize:16,
        fontFamily:'Poppins-Regular'
    },
})