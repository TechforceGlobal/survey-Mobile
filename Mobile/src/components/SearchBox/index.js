import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
export class SearchBox extends Component {
  render() {
    return (
          <View style={styles.searchBox}>
            {/* <AntDesign name="search1" size={30} color={'gray'}/> */}
            <Image source={require('../../assets/images/Search.png')} style={{height:22,width:22,resizeMode:'contain'}} />
            <TextInput
                placeholder={`Search ${this.props.for}`}
                value={this.props.searchValue}
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(searchValue) => this.props.search(searchValue)}
              />
          </View>
    )
  }
}

export default SearchBox;