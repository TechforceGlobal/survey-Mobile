import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground,ActivityIndicator } from 'react-native';
import { styles } from './styles';
export class Survey_Loader extends Component {

  render() {
    return (
          <View style={styles.MainIndicatoreView}>
            <View style={styles.BlackOpocityView} />
            <View style={styles.innerContainerView}>
              <ActivityIndicator
                color={'#165a87'}
              />
              <Text style={styles.loadingText}>{this.props.loadingText}</Text>
            </View>
          </View>
           
        

    )
  }
}

export default Survey_Loader;