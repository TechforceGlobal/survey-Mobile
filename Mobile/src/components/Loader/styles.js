import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width
export const styles = StyleSheet.create({
    MainIndicatoreView:{
        flex:1, 
        position: 'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0 
    },
    BlackOpocityView:{ 
        backgroundColor:'#000',
        opacity: 0.2, 
        position: 'absolute', 
        top:0,
        bottom:0,
        left:0,
        right:0 
    },
    innerContainerView:{ 
        height: 90, 
        width: '45%',
        justifyContent:'center',
        backgroundColor:'#fff',
        marginTop:height/2 - 45,
        alignSelf:'center',
        alignItems:'center',
        elevation:3
    },
    loadingText:{
        color:'#165a87',
        fontFamily:'Poppins-Regular'
    }
})