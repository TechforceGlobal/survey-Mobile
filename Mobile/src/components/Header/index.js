import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import { WebView } from 'react-native-webview';
export class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.menuContainer} onPress={() => global.mynavigation.openDrawer()}>
          {/* <Icon name="menu" size={30} color={'#165a87'}/> */}
          <Image source={require('../../assets/Icons/menu.png')} style={{ height: 30, width: 25 }} />
        </TouchableOpacity>
        <View style={styles.titleContainer}>
          <Text style={styles.headerText}>{this.props.headerName}</Text>
          {/* <View style={{ height: 50, width: 250, justifyContent: 'center' }}>
            <WebView source={{ html: `<h2 style="font-size:80px;text-align:center;padding-top:40px;color:#fff;font-family:Poppins-Bold">${this.props.headerName}</h2>` }}
              style={{ backgroundColor: 'transparent' }}
            />
          </View> */}
        </View>
      </View>
    )
  }
}

export default Header;