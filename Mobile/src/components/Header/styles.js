import { View, Text, NativeModules, Dimensions, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export const styles = StyleSheet.create({
    header:{
        height:50,
        backgroundColor:'#155f90',
        flexDirection:'row',
        justifyContent:'center'
    },
    menuContainer:{
        position:'absolute',
        left:10,
        top:10,
        backgroundColor:'#fff',
        paddingHorizontal:5,
        elevation:5,
    },
    titleContainer:{
        justifyContent:'center',
        alignItems:'center'
    },
    headerText:{
        marginTop:5,
        color:'#fff',
        fontSize:20,
        fontFamily:'Poppins-Bold'
    }
})