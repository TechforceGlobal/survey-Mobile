import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, Alert, TouchableOpacity, ImageBackground } from 'react-native';
import { styles } from './styles';
import { openDatabase } from 'react-native-sqlite-storage';
import { Loader } from '../Loader'
var db = openDatabase({ name: 'surveyIDG.db', createFromLocation: 1 });
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import { StackActions, NavigationActions } from 'react-navigation';
import { api_url } from '../../common'

export class Drawer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      token: null,
      email: '',
      savedRes: ''
    }
  }

  componentDidMount() {
    /* let CompletedSurvey = [];
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM CompletedSurvey',
        [],
        (tx, results) => {

          for (let k = 0; k < results.rows.length; k++) {
            CompletedSurvey.push(results.rows.item(k));
          }
          alert('CompletedSurvey: ' + JSON.stringify(CompletedSurvey));
          this.setState({ savedRes: CompletedSurvey.length })
        }
      )
    }) */
  }

  loadCommonData = async (call) => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key')
      const email = await AsyncStorage.getItem('@email')
      if (value !== null) {
        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            this.uploadTo_api(value, email, call)
          } else {
            alert("You do not have an internet connection");
          }
        });
      }
    } catch (e) {
      console.log('erro from Surveylist asynce' + JSON.stringify(e))
    }
  }

  uploadTo_api = (token, email, call) => {
    this.setState({ loading: true })
    let CompletedSurvey = [];
    let bodyData = [];
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM CompletedSurvey',
        [],
        (tx, results) => {

          for (let k = 0; k < results.rows.length; k++) {
            CompletedSurvey.push(results.rows.item(k));
          }
          console.log('CompletedSurvey: ' + JSON.stringify(CompletedSurvey));
          if (CompletedSurvey.length !== 0 || call === 'logout') {
            for (let i = 0; i < CompletedSurvey.length; i++) {
              bodyData.push(JSON.parse(CompletedSurvey[i].AnswerJSON))
            }
            console.log('bodyData: ' + JSON.stringify(bodyData))

            var options = {
              method: 'POST',
              body: JSON.stringify(bodyData),
              headers: {
                'Content-Type': 'application/json',
                'token': token
              }
            }

            fetch(api_url + 'storesurvey', options).
              then((response) =>
                response.json().
                  then((body) => {
                    console.log('Save api res: ' + JSON.stringify(body))
                    this.clearDb(email, call)
                  }).catch((error) =>
                    this.setState({
                      loading: false
                    }, () => {
                      console.log("API ERROR_RESPONSE: ", JSON.stringify(error))
                    })
                  )
              ).catch((error) => {
                this.setState({
                  loading: false
                }, () => {
                  console.log("API ERROR_OUTER: ", JSON.stringify(error));
                  this.setState({ loading: false }, () => {
                    Alert.alert(
                      '',
                      'Oops, Something went wrong',
                      [
                        { text: 'OK', onPress: () => console.log('something went wrong') },
                      ],
                      { cancelable: false }
                    )
                  })
                })
              })
          } else {
            Alert.alert(
              '',
              'No local survey data found. Are you sure you want to refresh survey?',
              [
                { text: 'Yes', onPress: () => this.clearDb(email, call) },
                { text: 'No', onPress: () => { this.setState({ loading: false }) } }
              ],
            )
          }
        }
      );
    });
  }

  clearDb = (email, call) => {
    //deleting Survey
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM Survey',
        [],
        (tx, results) => {

          db.transaction(tx => {
            tx.executeSql(
              'DELETE FROM SubSurvey',
              [],
              (tx, results) => {

                //deleting Company
                db.transaction(tx => {
                  tx.executeSql(
                    'DELETE FROM Company',
                    [],
                    (tx, results) => {
                      //deleting CompanyLocation
                      db.transaction(tx => {
                        tx.executeSql(
                          'DELETE FROM CompanyLocation',
                          [],
                          (tx, results) => {
                            //deleting Language
                            db.transaction(tx => {
                              tx.executeSql(
                                'DELETE FROM Language',
                                [],
                                (tx, results) => {
                                  //deleting common
                                  db.transaction(tx => {
                                    tx.executeSql(
                                      'DELETE FROM common',
                                      [],
                                      (tx, results) => {
                                        //deleting Question
                                        db.transaction(tx => {
                                          tx.executeSql(
                                            'DELETE FROM Question',
                                            [],
                                            (tx, results) => {

                                              db.transaction(tx => {
                                                tx.executeSql(
                                                  'DELETE FROM assets',
                                                  [],
                                                  (tx, results) => {
                                                    //deleting CompletedSurvey
                                                    db.transaction(tx => {
                                                      tx.executeSql(
                                                        'DELETE FROM CompletedSurvey',
                                                        [],
                                                        (tx, results) => {
                                                          this.setState({ loading: false }, () => {
                                                            global.mynavigation.closeDrawer()
                                                            if (call === 'async') {
                                                              const resetAction = StackActions.reset({
                                                                index: 0,
                                                                actions: [NavigationActions.navigate({ routeName: 'SurveyList', params: { surveyoremail: email } })],
                                                              });
                                                              this.props.navigation.dispatch(resetAction);
                                                            } else {
                                                              this.clearToken()
                                                            }
                                                          })
                                                        }
                                                      );
                                                    });
                                                  }
                                                );
                                              });
                                            }
                                          );
                                        });
                                      }
                                    );
                                  });
                                }
                              );
                            }
                            );
                          });
                      });
                    }
                  );
                });
              }
            );
          });
        }
      );
    });
  }

  on_async = () => {
    Alert.alert(
      '',
      'Are you sure you want to submit your offline survey responses?',
      [
        { text: 'Yes', onPress: () => this.loadCommonData('async') },
        { text: 'No', onPress: () => { } }
      ],
      { cancelable: false }
    )
  }

  on_logout = () => {
    this.loadCommonData('logout')
  }

  clearToken = () => {

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'SurveyList', params: { surveyoremail: email } })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  clearToken = async () => {
    const keys = ['@storage_Key', '@email']
    try {
      await AsyncStorage.multiRemove(keys)
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login' })],
      });
      this.props.navigation.dispatch(resetAction);
    } catch (e) {
      // remove error
    }
  }

  render() {
    return (
      <View style={styles.drawer}>
        {
          this.state.loading &&
          <Loader />
        }
        <TouchableOpacity style={styles.drawerItem} onPress={() => this.on_async()}>
          <Image source={(require('../../assets/Icons/sync2.png'))} style={styles.icon} />
          <Text style={styles.drawerItemText}>Sync</Text>
          <Text style={styles.badgeText}>{global.savedRes}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.drawerItem, { marginTop: 10 }]} onPress={() => this.on_logout()}>
          <Image source={(require('../../assets/Icons/signout.png'))} style={styles.icon} />
          <Text style={styles.drawerItemText}>Logout</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

export default Drawer;