import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
export class Breadcumb extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            emailError: false,
            passwordError: false,
            showHide_passord: true,
        }
    }

    navigate = (index) => {
        if(index===0){
            global.mynavigation.pop(1)
        }else{
            global.mynavigation.pop(index)
        }
    }

    render() {
        const {routes} = this.props;
        return (
            <View style={{flexDirection:'row',marginHorizontal:12}}>
                {
                    routes.map((item, index) => {
                        return (
                            <View style={{flexDirection:'row',alignItems:'center',marginTop:12,marginRight: 8}}>
                                <TouchableOpacity style={{marginRight:8}} onPress={()=>this.navigate(index)}>
                                    <Text style={{color:(index!==routes.length-1 || index===0) ?'#165a87' : '#404041',fontSize:13,fontFamily:'Poppins-Regular'}}>{item}</Text>
                                </TouchableOpacity>
                                {(index!==routes.length-1 || index===0) && 
                                    // <FontAwesome name="angle-right" size={15} color={'#165a87'} />
                                    <Image source={require('../../assets/images/blueaero.png')} style={{height:10,width:10,resizeMode:'contain'}} />
                                }
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}

export default Breadcumb;