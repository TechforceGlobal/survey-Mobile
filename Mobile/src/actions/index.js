import NetInfo from "@react-native-community/netinfo";
//import Toast from 'react-native-simple-toast';

const baseUrl = 'https://mozayada.com/api/v1/'

export const displayUserName = (username) => {
  return (dispatch, getState) => {
    dispatch({ type: 'DISPLAY_USERNAME', username })
  }
}

export const logout = (username) => {
  return (dispatch, getState) => {
    dispatch({ type: 'LOGOUT_SUCCESS' })
  }
}

export const changeLanguage = (language_code) => {
  return (dispatch, getState) => {
    dispatch({ type: 'CHANGE_LANGUAGE', language_code })
  }
}

export const register = (lang_code="en",first_name, last_name, mobile, email, password, password_confirmation) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'REGISTER_PENDING' })

        var url = baseUrl + 'register';
        var params = {
          "device_type": "2",
          "first_name": first_name,
          "last_name": last_name,
          "mobile": mobile,
          lang_code,
          "email": email,
          "password": password,
          "password_confirmation": password_confirmation
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'
          }
        }
        console.log('REGISTER options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('REGISTER API sucess: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'REGISTER_SUCCESS', body })
                } else {
                  dispatch({ type: 'REGISTER_FAILD', error: body.message })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("REGISTER API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const login = (lang_code="en",email, password, type, device_type, device_uuid, device_no, device_name, device_platform, device_model, device_version) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        //Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'LOGIN_PENDING' })

        var url = baseUrl + 'login';
        var params = {
          //"email":email,
          lang_code,
          device_type,
          device_uuid,
          device_no,
          device_name,
          device_platform,
          device_model,
          device_version,
          //"email":email,
          // "device_type": "1",
          // "device_uuid": "123456",
          // "device_no": "1234567",
          // "device_name": "test",
          // "device_platform": "test platform",
          // "device_model": "test model",
          // "device_version": "1.0"
        }
        if (type === "social_login") {
          params.email = email;
          params.social_login = "1";
        }
        else if (type === 'email') {
          params.email = email;
          params.password = password
        } else {
          params.mobile = email
          params.password = password
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'
          }
        }
        console.log('REGISTER options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('LOGIN API sucess: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'LOGIN_SUCCESS', body })
                } else {
                  dispatch({ type: 'LOGIN_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("LOGIN API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getCategoryData = (lang_code="en",buyer_id = '') => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETCATAGORY_PENDING' })

        var url = baseUrl + 'get-category-data';
        var params = {
          device_type: 2,
          lang_code,
          "show_product_count": 1,
          "show_breadcrumb": "1",
          "cat_ids": "",
          "buyer_id": buyer_id,
          "search_arr": {
            "auction_status": "",
            "only_parent": "1",
            "parent_id": "",
            "is_wishlist": 0
          }
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'
          }
        }
        console.log('GETCATAGORY options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GETCATAGORY API sucess in status MAin 200: ' + JSON.stringify(body.data));
                  dispatch({ type: 'GETCATAGORY_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETCATAGORY_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETCATAGORY API ERROR_OUTER: ", error);
          });
      }
    });

  }
}

export const getSubCategoryData = (lang_code="en",buyer_id = '', parent_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETSUBCATAGORY_PENDING' })

        var url = baseUrl + 'get-category-data';
        var params = {
          device_type: 2,
          lang_code,
          "show_product_count": 1,
          "show_breadcrumb": "1",
          "cat_ids": "",
          "buyer_id": buyer_id,
          "search_arr": {
            "auction_status": "",
            "only_parent": "0",
            parent_id,
            "is_wishlist": 0
          }
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'
          }
        }
        console.log('GET SUBCATAGORY options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GETSUBCATAGORY API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETSUBCATAGORY_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETSUBCATAGORY_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETCATAGORY API ERROR_OUTER: ", error);
          });
      }
    });

  }
}

export const getallauctionlist = (lang_code="en",status="1",category_id = "", sort = 0, sortdir = 0, auction_name = "", auction_type = "", condition = "", from_price = "", to_price = "", all_listing = "", only_parent = "1",page='1'/* ,dataDirection='next' */) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        //Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETALLAUCTION_PENDING' })
        var url = baseUrl + 'get-all-auction-list';
        var params = {
          device_type: 2,
          lang_code,
          "search_arr": {
            category_id,
            // "category_id": "",
            only_parent,
            auction_name,
            auction_type,
            //"status": "1",
            status,
            condition,
            "is_wishlist": "0",
            from_price,
            to_price,
            all_listing,
            sort,
            sortdir,
          },
          page
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'
          }
        }
        console.log('GETALLAUCTION options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GET_ALLAUCTION API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETALLAUCTION_SUCCESS', body/* ,dataDirection */ })
                } else {
                  dispatch({ type: 'GETALLAUCTION_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETALLAUCTION API ERROR_OUTER: ", error);
          });
      }
    });
  }

}

export const getallCategorylistintrval = (lang_code="en",buyer_token, category_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETCETOGORYLISTINTERVAL_PENDING' })

        var url = baseUrl + 'list-current-bid-amount ';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": category_id
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETCETOGORYLISTINTERVAL options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GETCETOGORYLISTINTERVAL API sucess in status 200: ' + JSON.stringify(body.data));
                  dispatch({ type: 'GETCETOGORYLISTINTERVAL_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETCETOGORYLISTINTERVAL_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETCETOGORYLISTINTERVAL API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const acutiondetail = (lang_code="en",auction_id, buyer_id = '') => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETAUCTION_PENDING' })

        var url = baseUrl + 'get-auction-details';
        var params = {
          device_type: 2,
          lang_code,
          "auction_id": `${auction_id}`,
          buyer_id
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'

          }
        }
        console.log('GETAUCTION options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GETAUCTION API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETAUCTION_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETAUCTION_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETAUCTION API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const submitauctionbid = (lang_code="en",auction_id, buyer_token, buyer_id, bidValue, bid_type) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'SETAUCTIONBID' })

        var url = baseUrl + 'auction-bid';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
          "bid_amount": `${bidValue}`,
          "bid_type": `${bid_type}`
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('SETAUCTIONBID options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                console.log('SETAUCTIONBID API sucess in status 200: ' + JSON.stringify(body));
                dispatch({ type: 'SETAUCTIONBID_SUCCESS', body })

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("SETAUCTIONBID API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const currentbidamount = (lang_code="en",auction_id, buyer_token, buyer_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETCURRENTBIDAMOUNT_PENDING' })

        var url = baseUrl + 'current-bid-amount';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETCURRENTBIDAMOUNT options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GETCURRENTBIDAMOUNT API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETCURRENTBIDAMOUNT_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETCURRENTBIDAMOUNT_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETCURRENTBIDAMOUNT API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const currentproxybidamount = (lang_code="en",auction_id, buyer_id, buyer_token, proxy_amount) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETCURRENTPROXYBIDAMOUNT_PENDING' })

        var url = baseUrl + 'submit-proxy-bid';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
          "proxy_amount": `${proxy_amount}`
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETCURRENTPROXYBIDAMOUNT options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                dispatch({ type: 'GETCURRENTPROXYBIDAMOUNT_SUCCESS', body })
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETCURRENTPROXYBIDAMOUNT API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const historybid = (lang_code="en",auction_id, buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETHISTORYBID_PENDING' })

        var url = baseUrl + 'get-auction-bid-history';
        var params = {
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
          lang_code,

        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETHISTORYBID options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('GETHISTORYBID API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETHISTORYBID_SUCCESS', body })
                } else {
                  console.log('GETHISTORYBID API fail: ' + JSON.stringify(body));
                  dispatch({ type: 'GETHISTORYBID_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETHISTORYBID API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const addToWishList = (lang_code="en",auction_id, buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'SETWISTLIST_PENDING' })

        var url = baseUrl + 'add-remove-wish-list';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('SETWISTLIST options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('SETWISTLIST API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'SETWISTLIST_SUCCESS', body })
                } else {
                  console.log('SETWISTLIST API sucess fail: ' + JSON.stringify(body));
                  dispatch({ type: 'SETWISTLIST_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("SETWISTLIST API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getallwishlistitem = (lang_code="en",buyer_id, buyer_token, sort = "1") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETALLWISHLISTITEM_PENDING' })

        var url = baseUrl + 'get-my-bid-list';
        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
          "filter_by": "is_watching",
          sort,
          "page": 1
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETALLWISHLISTITEM options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('GETALLWISHLISTITEM API sucess  ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'GETALLWISHLISTITEM_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETALLWISHLISTITEM_FAILD', error: body.message[0] })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETALLWISHLISTITEM API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const depositcheckout = (lang_code="en",auction_id, buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'DEPOSITCHECKOUT_PENDING' })

        var url = baseUrl + 'deposit-checkout ';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                if (body.status_code === 200) {
                  console.log('DEPOSITCHECKOUT API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'DEPOSITCHECKOUT_SUCCESS', body })
                } else {
                  dispatch({ type: 'DEPOSITCHECKOUT_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("DEPOSITCHECKOUT API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const submitdepositcheckout = (lang_code="en",buyer_token, buyer_id, auction_id, totaldeposit, paymethod, pay_cheque_no, pay_cheque_date, pay_bank_name, pay_bank_transfer_attachment, pay_cheque_attachment, pay_bank_transaction_no, session_id = "", payment_token = "") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'SUBMITDEPOSITCHECKOUT_PENDING' })

        var url = baseUrl + 'submit-deposit-checkout';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": auction_id,
          "buyer_id": buyer_id,
          "wallet_amount": 0,
          "device_type": "2",
          "deposit_amount": totaldeposit,
          "payment_data": {
            "method": paymethod
          },
        }
        let payment_data = {};

        if (paymethod === 'CHQ') {
          payment_data = {
            "method": "CHQ",
            pay_cheque_no,
            pay_cheque_date,
            pay_bank_name,
            pay_cheque_attachment
          }
        } else if (paymethod === 'BT') {
          payment_data = {
            "method": "BT",
            pay_bank_transaction_no,
            pay_bank_transfer_attachment
          }
        } else if (paymethod === 'WLT') {
          payment_data = {
            "method": "WLT"
          }
        } else if (paymethod === 'CASH') {
          payment_data = {
            "method": "CASH"
          }
        } else if (paymethod === 'CC') {
          payment_data = {
            "method": "CC",
            session_id,
            payment_token,
            "save_card": 0,
            "credit_type": "2"
          }
        }
        params.payment_data = payment_data;

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  dispatch({ type: 'SUBMITDEPOSITCHECKOUT_SUCCESS', body })
                } else {
                  dispatch({ type: 'SUBMITDEPOSITCHECKOUT_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          });
      }
    });
  }
}

export const auctionsellerquestion = (lang_code="en",auction_id, buyer_id, seller_id, question, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'AUCTIONSELLERQUESTION_PENDING' })

        var url = baseUrl + 'post-auction-seller-question';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
          "seller_id": `${seller_id}`,
          "question": `${question}`
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('AUCTIONSELLERQUESTION options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('AUCTIONSELLERQUESTION API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'AUCTIONSELLERQUESTION_SUCCESS', body })
                } else {
                  dispatch({ type: 'AUCTIONSELLERQUESTION_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("AUCTIONSELLERQUESTION API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const checkoutbuynow = (lang_code="en",auction_id, buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'AUCTIONCHECKOUT_PENDING' })

        var url = baseUrl + 'buy-now-item';
        var params = {
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
          "device_type": "2",
          lang_code,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('AUCTIONCHECKOUT options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('AUCTIONCHECKOUT API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'AUCTIONCHECKOUT_SUCCESS', body })
                } else {
                  dispatch({ type: 'AUCTIONCHECKOUT_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("AUCTIONCHECKOUT API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const buycheckoutnow = (lang_code="en",sales_quote_id, buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'BUYNOWCHECKOUT_PENDING' })

        var url = baseUrl + 'buy-now-checkout';
        var params = {
          device_type: 2,
          lang_code,
          "sales_quote_id": `${sales_quote_id}`,
          "buyer_id": `${buyer_id}`,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('BUYNOWCHECKOUT options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('BUYNOWCHECKOUT API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'BUYNOWCHECKOUT_SUCCESS', body })
                } else {
                  dispatch({ type: 'BUYNOWCHECKOUT_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("BUYNOWCHECKOUT API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getitemshipping = (lang_code="en",auction_id, address_type, address_id, country_code, state_code, buyer_id, sales_quote_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETITEMSHIPPING_PENDING' })

        var url = baseUrl + 'get-item-vat-and-shipping';
        var params = {};
        if (address_type === 1) {
          params = {
            device_type: 2,
            lang_code,
            auction_id,
            address_type,
            address_id,
            country_code,
            state_code,
            buyer_id,
            sales_quote_id
          }
        } else {
          params = {
            device_type: 2,
            lang_code,
            auction_id,
            address_type,
            country_code,
            state_code,
            buyer_id,
            sales_quote_id
          }
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETITEMSHIPPING options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('GETITEMSHIPPING API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'GETITEMSHIPPING_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETITEMSHIPPING_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETITEMSHIPPING API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const buyaddresslist = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'BUYERADDRESSLIST_PENDING' })

        var url = baseUrl + 'buyer-address-list';
        var params = {
          device_type: 2,
          lang_code,
          "buyer_id": `${buyer_id}`
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('BUYERADDRESSLIST options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('BUYERADDRESSLIST API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'BUYERADDRESSLIST_SUCCESS', body })
                } else {
                  dispatch({ type: 'BUYERADDRESSLIST_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("BUYERADDRESSLIST API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const buynoworder = (lang_code="en",buyer_token, sales_quote_id, buyer_id, auction_id, auction_type, auction_name, seller_id, shipping_type, total,
  first_name, last_name, email, address1, address2, phone1_isd, phone1, mobile1_isd, mobile1, country_code, state_code, city_code, zip,
  paymethod, address_type,
  pay_cheque_no,
  pay_cheque_date,
  pay_bank_name,
  pay_cheque_attachment,
  pay_bank_transaction_no,
  pay_bank_transfer_attachment,
  session_id = '',
  payment_token = ''
) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'BUYNOWORDER_PENDING' })


        var url = baseUrl + 'buy-now-order';
        var params = {
          "device_type": "2",
          lang_code,
          sales_quote_id,
          buyer_id,
          auction_id,
          auction_type,
          "is_address_exist": 0,
          auction_name,
          seller_id,
          shipping_type,
          "shipping_pickup_address": "",
          total,
          "address": {
            first_name,
            last_name,
            email,
            address_type,
            address1,
            address2,
            phone1_isd,
            phone1,
            mobile1_isd,
            mobile1,
            zip,
            country_code,
            state_code,
            city_code
          },
          "payment_data": {
            "method": paymethod
          },
        }
        let payment_data = {};

        if (paymethod === 'CHQ') {
          payment_data = {
            "method": "CHQ",
            pay_cheque_no,
            pay_cheque_date,
            pay_bank_name,
            pay_cheque_attachment
          }
        } else if (paymethod === 'BT') {
          payment_data = {
            "method": "BT",
            pay_bank_transaction_no,
            pay_bank_transfer_attachment
          }
        } else if (paymethod === 'WLT') {
          payment_data = {
            "method": "WLT"
          }
        } else if (paymethod === 'CASH') {
          payment_data = {
            "method": "CASH"
          }
        } else if (paymethod === 'CC') {
          payment_data = {
            "method": "CC",
            session_id,
            payment_token,
            "save_card": 0,
            "credit_type": "2"
          }
        }

        params.payment_data = payment_data

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('BUYNOWORDER options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('BUYNOWORDER API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'BUYNOWORDER_SUCCESS', body })
                } else {
                  dispatch({ type: 'BUYNOWORDER_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("BUYNOWORDER API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const deleteQueueItem = (lang_code = 'en', auction_id, sales_quote_id, buyer_id, buyer_token) => {

  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'DELETE_QUEUEITEM_PENDING' })


        var url = baseUrl + 'delete-buy-now-quote-item';
        var params = {
          device_type: 2,
          lang_code,
          auction_id,
          sales_quote_id,
          buyer_id
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('DELETE_QUEUEITEM options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('DELETE_QUEUEITEM API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'DELETE_QUEUEITEM_SUCCESS', body })
                } else {
                  dispatch({ type: 'DELETE_QUEUEITEM_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("DELETE_QUEUEITEM API ERROR_OUTER: ", error);
          });
      }
    });
  }
}


export const getcountrylist = (lang_code="en") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETCOUNTRY_PENDING' })


        var url = baseUrl + 'get-master-data';
        var params = {
          device_type: 2,
          "data_request": ["country"],
          lang_code

        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        console.log('GETCOUNTRY options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('GETCOUNTRY API sucess in status 200k: ' + JSON.stringify(body));
                  dispatch({ type: 'GETCOUNTRY_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETCOUNTRY_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETCOUNTRY API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getstatelist = (lang_code="en",countryCode) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'GETSTATE_PENDING' })

        var url = baseUrl + 'get-master-data';
        var params = {
          device_type: 2,
          "data_request": ["state"],
          lang_code,
          "country_code": countryCode

        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        console.log('GETSTATE options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('GETSTATE API sucess in status 200k: ' + JSON.stringify(body));
                  dispatch({ type: 'GETSTATE_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETSTATE_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETSTATE API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getcitylist = (lang_code="en",countrycode, statecode) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'GETCITY_PENDING' })

        var url = baseUrl + 'get-master-data';
        var params = {
          device_type: 2,
          "data_request": ["city"],
          lang_code,
          "country_code": `${countrycode}`,
          "state_code": `${statecode}`
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        console.log('GETCITY options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('GETCITY API sucess in status 200k: ' + JSON.stringify(body));
                  dispatch({ type: 'GETCITY_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETCITY_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETCITY API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getisdlist = (lang_code="en") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'GETISD_PENDING' })

        var url = baseUrl + 'get-master-data';
        var params = {
          device_type: 2,
          "data_request": ["mobile_isd"],
          lang_code
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        console.log('GETISD options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('GETISD API sucess in status 200k: ' + JSON.stringify(body));
                  dispatch({ type: 'GETISD_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETISD_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETISD API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const submitcloseEnvelopebid = (lang_code="en",auction_id, buyer_token, buyer_id, bidValue) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'SUBMIT_CLOSE_ENVELOPE_PENDING' })

        var url = baseUrl + 'submit-close-envelope-bid';
        var params = {
          lang_code,
          device_type: 2,
          "auction_id": `${auction_id}`,
          "buyer_id": `${buyer_id}`,
          "bid_amount": `${bidValue}`,
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('SUBMIT_CLOSE_ENVELOPE options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                dispatch({ type: 'SUBMIT_CLOSE_ENVELOPE_SUCCESS', body })
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("SUBMIT_CLOSE_ENVELOP API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getprivateauctionlist = (lang_code="en",buyer_id, buyer_token, sort = "2") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'GET_PRIVATE_AUCTION_LIST_PENDING' })

        var url = baseUrl + 'get-private-auction';
        var params = {
          device_type: 2,
          lang_code,
          buyer_id,
          sort,
          "page": "1"
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GET_PRIVATE_AUCTION_LIST options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('GET_PRIVATE_AUCTION_LIST API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GET_PRIVATE_AUCTION_LIST_SUCCESS', body })
                } else {
                  dispatch({ type: 'GET_PRIVATE_AUCTION_LIST_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GET_PRIVATE_AUCTION_LIST API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getaddresslistdata = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETBUYERADDRESSLIST_PENDING' })


        var url = baseUrl + 'buyer-address-list';
        var params = {
          device_type: 2,
          lang_code,
          buyer_id,
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETBUYERADDRESSLIST options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log('GETBUYERADDRESSLIST API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETBUYERADDRESSLIST_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETBUYERADDRESSLIST_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETBUYERADDRESSLIST API ERROR_OUTER: ", error);
          });
      }
    });
  }
}


export const addbuyreraddresslistdata = (lang_code="en",buyer_id, buyer_token, fname, lname, address1, address2, phno, phisd, mono, moisd, zip, countrycode, statecode, citycode) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'ADDBUYERADDRESS_PENDING' })


        var url = baseUrl + 'add-buyer-address';
        var params = {
          lang_code,
          "device_type": "2",
          buyer_id,
          "first_name": `${fname}`,
          "last_name": `${lname}`,
          "address1": `${address1}`,
          "address2": `${address2}`,
          "phone1_isd": `${phisd}`,
          "phone1": `${phno}`,
          "mobile1_isd": `${moisd}`,
          "mobile1": `${mono}`,
          "zip": `${zip}`,
          "country_code": `${countrycode}`,
          "state_code": `${statecode}`,
          "city_code": `${citycode}`
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('ADDBUYERADDRESS options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('ADDBUYERADDRESS API sucess in status 200: ' + JSON.stringify(body));

                  dispatch({ type: 'ADDBUYERADDRESS_SUCCESS', body })
                } else {
                  console.log('ADDBUYERADDRESS API FAIL in status 400: ' + JSON.stringify(body));

                  dispatch({ type: 'ADDBUYERADDRESS_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("ADDBUYERADDRESS API ERROR_OUTER: ", error);
          });
      }
    });
  }
}
export const editbuyreraddresslistdata = (lang_code="en",buyer_id, buyer_token, address_id, fname, lname, address1, address2, phno, phisd, mono, moisd, zip, countrycode, statecode, citycode) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'EDITBUYERADDRESS_PENDING' })

        var url = baseUrl + 'post-buyer-address';
        var params = {
          lang_code,
          "device_type": "2",
          buyer_id,
          "address_id": `${address_id}`,
          "first_name": `${fname}`,
          "last_name": `${lname}`,
          "address1": `${address1}`,
          "address2": `${address2}`,
          "phone1_isd": `${phisd}`,
          "phone1": `${phno}`,
          "mobile1_isd": `${moisd}`,
          "mobile1": `${mono}`,
          "zip": `${zip}`,
          "country_code": `${countrycode}`,
          "state_code": `${statecode}`,
          "city_code": `${citycode}`
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('EDITBUYERADDRESS options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('EDITBUYERADDRESS API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'EDITBUYERADDRESS_SUCCESS', body })
                } else {
                  console.log('EDITBUYERADDRESS API FAIL in status 400: ' + JSON.stringify(body.message));
                  dispatch({ type: 'EDITBUYERADDRESS_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("EDITBUYERADDRESS API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const setprimaryaddress = (lang_code="en",buyer_id, buyer_token, address_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'SETPRIMARYADDRESS_PENDING' })


        var url = baseUrl + 'post-buyer-address';
        var params = {
          lang_code,
          device_type: 2,
          buyer_id,
          address_id,
          "set_default": "1",
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('SETPRIMARYADDRESS options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('SETPRIMARYADDRESS API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'SETPRIMARYADDRESS_SUCCESS', body })
                } else {
                  console.log('SETPRIMARYADDRESS API FAIL in status 400: ' + JSON.stringify(body.message));
                  dispatch({ type: 'SETPRIMARYADDRESS_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("SETPRIMARYADDRESS API ERROR_OUTER: ", error);
          });
      }
    });
  }
}


export const deletebuyeraddress = (lang_code="en",buyer_id, buyer_token, address_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'DELETEBUYERADDRESS_PENDING' })


        var url = baseUrl + 'delete-buyer-address';
        var params = {
          lang_code,
          device_type: 2,
          buyer_id,
          address_id,
          "set_default": "1",
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('DELETEBUYERADDRESS options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('DELETEBUYERADDRESS API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'DELETEBUYERADDRESS_SUCCESS', body })
                } else {
                  console.log('DELETEBUYERADDRESS API FAIL in status 400: ' + JSON.stringify(body.message));
                  dispatch({ type: 'DELETEBUYERADDRESS_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })

              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("DELETEBUYERADDRESS API ERROR_OUTER: ", error);
          });
      }
    });
  }
}


export const getcreditcardlist = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETCREDITCARDLIST_PENDING' })
        var url = baseUrl + 'buyer-credit-card-list';
        var params = {
          device_type: 2,
          lang_code,
          buyer_id,
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GETCREDITCARDLIST options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('GETCREDITCARDLIST API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'GETCREDITCARDLIST_SUCCESS', body })
                } else {
                  console.log('GETCREDITCARDLIST API FAIL in status 400: ' + JSON.stringify(body));
                  dispatch({ type: 'GETCREDITCARDLIST_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })

              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETCREDITCARDLIST API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const setprimarycard = (lang_code="en",buyer_id, buyer_token, card_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'SETPRIMARYCARD_PENDING' })


        var url = baseUrl + 'set-buyer-credit-card';
        var params = {
          lang_code,
          device_type: 2,
          buyer_id,
          card_id,
          "set_default": "1",
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('SETPRIMARYCARD options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('SETPRIMARYCARD API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'SETPRIMARYCARD_SUCCESS', body })
                } else {
                  console.log('SETPRIMARYCARD API FAIL in status 400: ' + JSON.stringify(body.message));
                  dispatch({ type: 'SETPRIMARYCARD_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("SETPRIMARYCARD API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const deletebuyercreditcard = (lang_code="en",buyer_id, buyer_token, card_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if(!isConnected){
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      }else{
        dispatch({ type: 'DELETEBUYERCREDITCARD_PENDING' })
        var url = baseUrl + 'delete-buyer-credit-card';
        var params = {
          lang_code,
          device_type: 2,
          buyer_id,
          card_id,
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('DELETEBUYERCREDITCARD options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('DELETEBUYERCREDITCARD API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'DELETEBUYERCREDITCARD_SUCCESS', body })
                } else {
                  console.log('DELETEBUYERCREDITCARD API FAIL in status 400: ' + JSON.stringify(body.message));
                  dispatch({ type: 'DELETEBUYERCREDITCARD_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("DELETEBUYERCREDITCARD API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const addcreditcarddata = (lang_code="en",buyer_id, buyer_token, session_id) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'ADDCREDITCARDDATA_PENDING' })

        var url = baseUrl + 'add-buyer-credit-card';
        var params = {
          lang_code,
          "device_type": "2",
          buyer_id,
          "card_type": "1",
          session_id
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('ADDCREDITCARDDATA options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log('ADDCREDITCARDDATA API sucess in status 200: ' + JSON.stringify(body));
                  dispatch({ type: 'ADDCREDITCARDDATA_SUCCESS', body })
                } else {
                  console.log('ADDCREDITCARDDATA API FAIL in status 400: ' + JSON.stringify(body));
                  dispatch({ type: 'ADDCREDITCARDDATA_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("ADDCREDITCARDDATA API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getinvoicelist = (lang_code="en",buyer_id, buyer_token, sort = "1") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETINVOICELIST_PENDING" })

        var url = baseUrl + "get-my-bid-list";

        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
          "filter_by": "is_invoice",
          sort,
          "page": 1
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETINVOICELIST option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("GETINVOICELIST API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETINVOICELIST_SUCCESS", body })
                } else {
                  console.log("GETINVOICELIST API Fail Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETINVOICELIST_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETINVOICELIST API Second Catch ERROR_OUTER: " + JSON.stringify(error))
          })
      }
    });
  }
}

export const getinvoicedetail = (lang_code="en",buyer_id, buyer_token, salesorderid) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: "GETINVOICEDETAIL_PENDING" })


        var url = baseUrl + "get-invoice-detail";

        var params = {
          device_type: 2,
          "buyer_id": `${buyer_id}`,
          "sales_order_id": `${salesorderid}`,
          lang_code
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETINVOICEDETAIL option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code === 200) {
                  console.log("GETINVOICEDETAIL API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETINVOICEDETAIL_SUCCESS", body })
                } else {
                  console.log("GETINVOICEDETAIL API Fail 200 Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETINVOICEDETAIL_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETINVOICEDETAIL API Second Try Catch ERROR_OUTER: " + JSON.stringify(error))
          })
      }
    });
  }
}

export const getwonlist = (lang_code="en",buyer_id, buyer_token, sort = "1") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETWONLIST_PENDING" })

        var url = baseUrl + "get-my-bid-list";

        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
          "filter_by": "is_won",
          sort,
          "page": 1
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETWONLIST option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("GETWONLIST API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETWONLIST_SUCCESS", body })
                } else {
                  console.log("GETWONLIST API Fail 200 Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETWONLIST_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          })
      }
    });
  }
}



export const getnotwonlist = (lang_code="en",buyer_id, buyer_token, sort = "1") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETNOTWON_PENDING" })

        var url = baseUrl + "get-my-bid-list";

        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
          "filter_by": "is_not_won",
          sort,
          "page": 1
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETNOTWON option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("GETNOTWON API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETNOTWON_SUCCESS", body })
                } else {
                  console.log("GETNOTWON API Fail 200 Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETNOTWON_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETNOTWON API Second Catch ERROR_OUTER: " + JSON.stringify(error))
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          })
      }
    });
  }
}

export const getactivelist = (lang_code="en",buyer_id, buyer_token, sort = "1") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETACTIVELIST_PENDING" })

        var url = baseUrl + "get-my-bid-list";

        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
          "filter_by": "is_active",
          sort,
          "page": 1
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETACTIVELIST option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("GETACTIVELIST API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETACTIVELIST_SUCCESS", body })
                } else {
                  console.log("GETACTIVELIST API Fail 200 Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETACTIVELIST_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GETACTIVELIST API Second Catch ERROR_OUTER: " + JSON.stringify(error))
          })
      }
    });
  }
}

export const editProfile = (buyer_id, first_name, last_name, mobile, email, dob, lang_code, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'EDITPROFILE_PENDING' })


        var url = baseUrl + 'edit-profile';
        var params = {
          "device_type": "2",
          buyer_id,
          "id": buyer_id,
          first_name,
          last_name,
          mobile,
          email,
          dob,
          lang_code,
          "image_url": "https://image.shutterstock.com/image-vector/blank-cheque-design-template-illustration-260nw-1183401064.jpg"
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('editProfile options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('editProfile API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'EDITPROFILE_SUCCESS', body })
                } else {
                  dispatch({ type: 'EDITPROFILE_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("editProfile API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getLanguages = () => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'GETLANGUAGES_PENDING' })

        var url = baseUrl + 'get-master-data';
        var params = { device_type: 2, "data_request": ["language"] }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        console.log('getLanguages options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('getLanguages API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'GETLANGUAGES_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETLANGUAGES_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("getLanguages API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getProfile = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GETPROFILE_PENDING' })

        var url = baseUrl + 'get-buyer-details';
        var params = { lang_code,device_type: 2, buyer_id }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': buyer_token
          }
        }
        console.log('getProfile options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('getProfile API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'GETPROFILE_SUCCESS', body })
                } else {
                  dispatch({ type: 'GETPROFILE_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })

              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("getLanguages API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const changePassword = (lang_code="en",current_password, password, password_confirmation, buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: 'CHANGEPASSWORD_PENDING' })

        var url = baseUrl + 'change-password';
        var params = {
          lang_code,
          "device_type": "2",
          buyer_id,
          current_password,
          password,
          password_confirmation
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('change-password options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('change-password API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'CHANGEPASSWORD_SUCCESS', body })
                } else {
                  dispatch({ type: 'CHANGEPASSWORD_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })

              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("change-password API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const forgotPassword = (lang_code="en",email) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'FORGOTPASSWORD_PENDING' })


        var url = baseUrl + 'forgot-password';
        var params = {
          lang_code,
          "device_type": "2",
          "email": email
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g'
          }
        }
        console.log('FORGOTPASSWORD options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('FORGOTPASSWORD API sucess: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'FORGOTPASSWORD_SUCCESS', body })
                } else {
                  dispatch({ type: 'FORGOTPASSWORD_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("FORGOTPASSWORD API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getPaymentHistory = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GET_PAYMENTHISTORY_PENDING' })


        var url = baseUrl + 'get-buyer-payment-history';
        var params = {
          lang_code,
          "device_type": "2",
          buyer_id
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            username: buyer_token
          }
        }
        console.log('get-buyer-payment-history options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                console.log('get-buyer-payment-history API sucess: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'GET_PAYMENTHISTORY_SUCCESS', body })
                } else {
                  dispatch({ type: 'GET_PAYMENTHISTORY_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("get-buyer-payment-history API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const addPaymentWallet = (lang_code="en",paymethod, buyer_id, amount, buyer_token, pay_bank_transfer_attachment, pay_bank_transaction_no, chq_no, chq_dt, bnk_name, chq_atch, session_id = "", payment_token = "") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'ADD_PAYMENT_WALLET_PENDING' })
        var url = baseUrl + 'add-payment-wallet';
        var params = {
          lang_code,
          "device_type": "2",
          "buyer_id": `${buyer_id}`,
          "amount": `${amount}`,
          "payment_data": {
            "method": paymethod
          },
        }
        let payment_data = {};
        if (paymethod === 'CASH') {
          payment_data = {
            "method": `${paymethod}`,
          }
        } else if (paymethod === 'BT') {
          payment_data = {
            "method": `${paymethod}`,
            pay_bank_transfer_attachment,
            "pay_bank_transaction_no": `${pay_bank_transaction_no}`
          }
        } else if (paymethod === 'CHQ') {
          payment_data = {
            "method": `${paymethod}`,
            "pay_cheque_no": `${chq_no}`,
            "pay_cheque_date": `${chq_dt}`,
            "pay_bank_name": `${bnk_name}`,
            "pay_cheque_attachment": `${chq_atch}`
          }
        } else if (paymethod === 'CC') {
          payment_data = {
            "method": 'CC',
            session_id,
            payment_token,
            "save_card": 0,
            "credit_type": "2"
          }
        }

        params.payment_data = payment_data

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('ADD_PAYMENT_WALLET options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                console.log('ADD_PAYMENT_WALLET API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  dispatch({ type: 'ADD_PAYMENT_WALLET_SUCCESS', body })
                } else {
                  dispatch({ type: 'ADD_PAYMENT_WALLET_FAILD', body })
                }
              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })

              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("ADD_PAYMENT_WALLET API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const getPaymentWalletHistory = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: 'GET_PAYMENT_WALLET_HISTORY_PENDING' })
        var url = baseUrl + 'get-buyer-wallet-history';
        var params = {
          "device_type": "2",
          buyer_id,
          lang_code,
        }

        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'username': `${buyer_token}`
          }
        }
        console.log('GET_PAYMENT_WALLET_HISTORY options: ' + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {

                console.log('GET_PAYMENT_WALLET_HISTORY API sucess in status 200: ' + JSON.stringify(body));
                if (body.status_code === 200) {
                  console.log('GET_PAYMENT_WALLET_HISTORY API sucess in status 200 IFFFFFFF : ' + JSON.stringify(body));
                  dispatch({ type: 'GET_PAYMENT_WALLET_HISTORY_SUCCESS', body })
                } else {
                  console.log('GET_PAYMENT_WALLET_HISTORY API sucess in status 200 elseeeeeee : ' + JSON.stringify(body));
                  dispatch({ type: 'GET_PAYMENT_WALLET_HISTORY_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })

              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
            console.log("GET_PAYMENT_WALLET_HISTORY API ERROR_OUTER: ", error);
          });
      }
    });
  }
}

export const auctionfeedback = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETFEEDBACK_PENDING" })

        var url = baseUrl + "get-user-auction-feedbacks";

        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETFEEDBACK option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("GETFEEDBACK API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETFEEDBACK_SUCCESS", body })
                }
                else {
                  console.log("GETFEEDBACK API Fail Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETFEEDBACK_FAILD', body })
                }

              })
              .catch((error) => {
                console.log("GETFEEDBACK API First Catch Error: " + JSON.stringify(error))
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETFEEDBACK API Second Catch ERROR_OUTER: " + JSON.stringify(error))
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          })
      }
    });
  }
}

export const getdashboard = (lang_code="en",buyer_id, buyer_token) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETDASHBOARD_PENDING" })

        var url = baseUrl + "get-buyer-account-detail";

        var params = {
          lang_code,
          device_type: 2,
          "buyer_id": `${buyer_id}`,
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
            'Username': `${buyer_token}`
          }
        }
        console.log("GETDASHBOARD option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("GETDASHBOARD API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETDASHBOARD_SUCCESS", body })
                }
                else {
                  console.log("GETDASHBOARD API Fail Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETDASHBOARD_FAILD', body })
                }

              })
              .catch((error) => {
                console.log("GETDASHBOARD API First Catch Error: " + JSON.stringify(error))
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETDASHBOARD API Second Catch ERROR_OUTER: " + JSON.stringify(error))
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          })
      }
    });
  }
}

export const getbuynowlist = (lang_code="en",status="1",category_id = "", sort = 0, sortdir = 0, auction_name = "", auction_type = "", condition = "", from_price = "", to_price = "", all_listing = "", only_parent = "1") => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "GETBUYNOWLIST_PENDING" })

        var url = baseUrl + "get-all-auction-list";

        var params = {
          device_type: 2,
          lang_code,
          "search_arr": {
            auction_type,
            category_id,
            only_parent,
            auction_name,
            status,
            condition,
            "is_wishlist": "0",
            from_price,
            to_price,
            all_listing,
            "is_accept_offer": "",
            sort,
            sortdir,
            "page": "1",
          }
        }
        var options = {
          method: 'POST',
          body: JSON.stringify(params),
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        console.log("GETBUYNOWLIST option:  " + JSON.stringify(options))
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  //console.log("GETBUYNOWLIST API Success 200: " + JSON.stringify(body))
                  dispatch({ type: "GETBUYNOWLIST_SUCCESS", body })
                }
                else {
                  console.log("GETBUYNOWLIST API Fail Error: " + JSON.stringify(body))
                  dispatch({ type: 'GETBUYNOWLIST_FAILD', body })
                }

              })
              .catch((error) => {
                console.log("GETBUYNOWLIST API First Catch Error: " + JSON.stringify(error))
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            console.log("GETBUYNOWLIST API Second Catch ERROR_OUTER: " + JSON.stringify(error))
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          })
      }
    });
  }
}

export const getAllLabels = () => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {
        dispatch({ type: "GETALLLABELS_PENDING" })
        var url = baseUrl + "get-variable-data";

        var options = {
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
            'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          }
        }
        fetch(url, options)
          .then((response) => {
            response.json()
              .then((body) => {
                if (body.status_code == 200) {
                  console.log("=======all translations: " + JSON.stringify(body) + '===================')
                  dispatch({ type: "GETALLLABELS_SUCCESS", body })
                }
                else {
                  dispatch({ type: 'GETALLLABELS_FAILD', body })
                }

              })
              .catch((error) => {
                dispatch({ type: 'SOMETHING_WENT_WRONG' })
              })
          })
          .catch((error) => {
            dispatch({ type: 'SOMETHING_WENT_WRONG' })
          })
      }
    });
  }
}
export const auctionsellerfeedback = (lang_code="en",buyer_id,buyer_token,seller_id,auction_id,ratting,comment) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "SETSELLERFEEDBACK_PENDING" })

        var url = baseUrl + "auction-seller-feedback";

        var params = {
          lang_code,
          device_type: 2,
          buyer_id,
          seller_id,
          auction_id,
          ratting,
          comment
      }
      var options = {
        method: 'POST',
        body: JSON.stringify(params),
        headers: {
          'Content-type': 'application/json',
          'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          'username':buyer_token
        }
      }
      console.log("SETSELLERFEEDBACK option:  " + JSON.stringify(options))
      fetch(url, options)
        .then((response) => {
          response.json()
            .then((body) => {
              if (body.status_code == 200) {
                console.log("SETSELLERFEEDBACK API Success 200: " + JSON.stringify(body))
                dispatch({ type: "SETSELLERFEEDBACK_SUCCESS", body })
              }
              else {
                console.log("SETSELLERFEEDBACK API Fail Error: " + JSON.stringify(body))
                dispatch({ type: 'SETSELLERFEEDBACK_FAILD', body })
              }

            })
            .catch((error) => {
              console.log("SETSELLERFEEDBACK API First Catch Error: " + JSON.stringify(error))
              dispatch({ type: 'SOMETHING_WENT_WRONG' })
            })
        })
        .catch((error) => {
          console.log("SETSELLERFEEDBACK API Second Catch ERROR_OUTER: " + JSON.stringify(error))
          dispatch({ type: 'SOMETHING_WENT_WRONG' })
        })
    }
    });
}
}
export const auctionsellercontect = (lang_code="en",buyer_id,buyer_token,auction_id,seller_id,subject,message) => {
  return (dispatch, getState) => {
    NetInfo.fetch().then(state => {
      let isConnected = state.isConnected;
      if (!isConnected) {
        Toast.show('Please check your internet connection!', Toast.SHORT, Toast.BOTTOM)
      } else {

        dispatch({ type: "SETSELLERCONTACT_PENDING" })

        var url = baseUrl + "auction-seller-contact";

        var params = {
          lang_code,
          device_type: 2,
          buyer_id,
          auction_id,
          seller_id,
          subject,
          message,
      }
      var options = {
        method: 'POST',
        body: JSON.stringify(params),
        headers: {
          'Content-type': 'application/json',
          'Authorization': '0ea4e3e69cb3bae2e3a7838db6ac66849a3908b8833c2d0e8d0504cd8bc8d31322812505356b4f8g',
          'username':buyer_token
        }
      }
      console.log("SETSELLERCONTACT option:  " + JSON.stringify(options))
      fetch(url, options)
        .then((response) => {
          response.json()
            .then((body) => {
              if (body.status_code == 200) {
                console.log("SETSELLERCONTACT API Success 200: " + JSON.stringify(body))
                dispatch({ type: "SETSELLERCONTACT_SUCCESS", body })
              }
              else {
                console.log("SETSELLERCONTACT API Fail Error: " + JSON.stringify(body))
                dispatch({ type: 'SETSELLERCONTACT_FAILD', body })
              }

            })
            .catch((error) => {
              console.log("SETSELLERCONTACT API First Catch Error: " + JSON.stringify(error))
              dispatch({ type: 'SOMETHING_WENT_WRONG' })
            })
        })
        .catch((error) => {
          console.log("SETSELLERCONTACT API Second Catch ERROR_OUTER: " + JSON.stringify(error))
          dispatch({ type: 'SOMETHING_WENT_WRONG' })
        })
    }
    });
}
}